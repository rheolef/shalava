#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
from tkinter import *
import tkinter.filedialog
from tkinter import messagebox
import os
import matplotlib.pyplot as plt
import shutil

dark_grey = "#282827"
light_grey = "#484845"

class Mymenu(Menu):
	def __init__(self, parent, *args, **kw):
		Menu.__init__(self, parent, *args, **kw)
		#creation of menu
		self.parent=parent
		self.config(bg=light_grey,fg="white" )
		parent.config(menu=self)
		menufichier = Menu(self,tearoff=0, bg=light_grey,fg="white")
		self.add_cascade(label="File", menu=menufichier)
		menufichier.add_command(label="Load data", command=self.open_file)
		menufichier.add_separator()
		menufichier.add_command(label="Save   (Ctrl+s)", command=self.save_file)
		menufichier.add_separator()
		menufichier.add_command(label="Exit   (Ctrl+q)", command=self.exit_command)
		parent.bind_all('<Control-s>', self.save_file)
	def save_file(self,event=None):
		my_file = tkinter.filedialog.asksaveasfile(mode='w', title="Save input file", filetypes=[('gdat files','.gdat')], defaultextension=".gdat", parent=self.parent)
		if my_file==None: # asksaveasfile return `None` if dialog closed with "cancel".
			return
		self.parent.p5.write_file(my_file)
		my_file.close()
		print("File saved in "+my_file.name)
	def open_file(self):
		vents_page=self.parent.p3.hot_spot_Frame
		forest_page=self.parent.p4.forest_characteristics_Frame
		simulation_page=self.parent.p5
		filename=tkinter.filedialog.askopenfilename(title="Open input file",filetypes=[('gdat files','.gdat')],parent=self.parent)
		if not filename: # askopenfilename return `false` if dialog closed with "cancel".
			return
		f=open(filename,"r")
		lines = f.readlines()
		f.close()
		my_dict1 = {}
		my_dict1["#south"] = self.parent.p1.south_variable
		my_dict1["#est"] = self.parent.p1.est_variable
		my_dict1["#north"] = self.parent.p1.north_variable
		my_dict1["#west"] = self.parent.p1.west_variable
		my_dict1["#reso"] = self.parent.p1.reso_variable
		my_dict1["#rho"] = self.parent.p2.rho_variable
		my_dict1["#mu0"] = self.parent.p2.mu_variable
		my_dict1["#tau"] = self.parent.p2.tau_variable
		my_dict2 = {}
		my_dict2["#nb_vents"] = vents_page.nb_vents
		for line in lines:
			name_input_value = line.split()[0]
			input_value = line.split()[1]
			if name_input_value=="#UTM_zone":
				self.parent.p1.utm_zone.Myvar.set(input_value)
			if name_input_value=="#DEM_filename":
				if input_value != "none":
					try:
						Q_f=open(input_value,"r") #[:-1]
					except:
						if messagebox.askyesno(title="Warning", message="DEM file not found. Update file ?", parent=self.parent):
							self.parent.p1.open_DEM_file()
					else:
						Q_f.close()
						self.parent.p1.update_DEM(input_value)
			if name_input_value in my_dict2:
				nb_vents_difference = int(input_value)-my_dict2[name_input_value].get()
				if nb_vents_difference>0:
					for j in range(nb_vents_difference):
						vents_page.command_button()
				elif nb_vents_difference<0:
					for j in range(-nb_vents_difference):
						vents_page.vent_frame_list[-1].command_rm_button(vents_page)
				filename_input_value = lines[lines.index(line)+1].split()[1]
				if filename_input_value == "none":
					vents_page.vent_frame_list[0].constant_Radio.select()
					vents_page.vent_frame_list[0].command_for_constant()
				else:
					vents_page.vent_frame_list[0].variable_Radio.select()
					vents_page.vent_frame_list[0].command_for_variable()
					try:
						Q_f=open(filename_input_value,"r") #[:-1]
					except:
						if messagebox.askyesno(title="Warning", message="Flow rate file (vent 1) not found. Update file ?", parent=self.parent):
							vents_page.vent_frame_list[0].open_flow_rate_file()
						else:
							vents_page.vent_frame_list[0].short_filename_Var.set("none")
							vents_page.vent_frame_list[0].flow_rate_filename = "none"
					else:
						Q_lines = Q_f.readlines()
						Q_f.close()
						vents_page.vent_frame_list[0].plot_Q_t(Q_lines)
						vents_page.vent_frame_list[0].short_filename_Var.set(filename_input_value.split("/")[-1]) #[:-1]
						vents_page.vent_frame_list[0].flow_rate_filename = filename_input_value #[1:-1]
				Q_input_value = lines[lines.index(line)+2].split()[1]
				R_input_value = lines[lines.index(line)+3].split()[1]
				xe_input_value = lines[lines.index(line)+4].split()[1]
				ye_input_value = lines[lines.index(line)+5].split()[1]
				ti_input_value = lines[lines.index(line)+6].split()[1]
				tf_input_value = lines[lines.index(line)+7].split()[1]
				self.parent.p1.main_vent_xe_variable.Myvar.set(xe_input_value)
				self.parent.p1.main_vent_ye_variable.Myvar.set(ye_input_value)
				vents_page.vent_frame_list[0].Q_variable.Myvar.set(Q_input_value)
				vents_page.vent_frame_list[0].R_variable.Myvar.set(R_input_value)
				vents_page.vent_frame_list[0].xe_variable.Myvar.set(xe_input_value)
				vents_page.vent_frame_list[0].ye_variable.Myvar.set(ye_input_value)
				vents_page.vent_frame_list[0].ti_variable.Myvar.set(ti_input_value)
				vents_page.vent_frame_list[0].tf_variable.Myvar.set(tf_input_value)
		for line in lines:
			name_input_value = line.split()[0]
			input_value = line.split()[1]
			if name_input_value in my_dict1:
				my_dict1[name_input_value].Myvar.set(input_value)
			if name_input_value in my_dict2:
				nb_vents_difference = int(input_value)-my_dict2[name_input_value].get()
				if nb_vents_difference>0:
					for j in range(nb_vents_difference):
						vents_page.command_button()
				elif nb_vents_difference<0:
					for j in range(-nb_vents_difference):
						vents_page.vent_frame_list[-1].command_rm_button(vents_page)
				for i in range(1,int(vents_page.nb_vents.get())):
					filename_input_value = lines[lines.index(line)+1+7*i].split()[1]
					if filename_input_value == "none":
						vents_page.vent_frame_list[i].constant_Radio.select()
						vents_page.vent_frame_list[i].command_for_constant()
					else:
						vents_page.vent_frame_list[i].variable_Radio.select()
						vents_page.vent_frame_list[i].command_for_variable()
						try:
							Q_f=open(filename_input_value,"r") #[:-1]
						except:
							if messagebox.askyesno(title="Warning", message="Flow rate file (vent "+str(i+1)+") not found. Update file ?", parent=self.parent):
								vents_page.vent_frame_list[i].open_flow_rate_file()
							else:
								vents_page.vent_frame_list[i].short_filename_Var.set("none")
								vents_page.vent_frame_list[i].flow_rate_filename = "none"
						else:
							Q_lines = Q_f.readlines()
							Q_f.close()
							vents_page.vent_frame_list[i].plot_Q_t(Q_lines)
							vents_page.vent_frame_list[i].short_filename_Var.set(filename_input_value.split("/")[-1]) #[:-1]
							vents_page.vent_frame_list[i].flow_rate_filename = filename_input_value #[1:-1]"""
					Q_input_value = lines[lines.index(line)+2+7*i].split()[1]
					R_input_value = lines[lines.index(line)+3+7*i].split()[1]
					xe_input_value = lines[lines.index(line)+4+7*i].split()[1]
					ye_input_value = lines[lines.index(line)+5+7*i].split()[1]
					ti_input_value = lines[lines.index(line)+6+7*i].split()[1]
					tf_input_value = lines[lines.index(line)+7+7*i].split()[1]
					vents_page.vent_frame_list[i].Q_variable.Myvar.set(Q_input_value)
					vents_page.vent_frame_list[i].R_variable.Myvar.set(R_input_value)
					vents_page.vent_frame_list[i].xe_variable.Myvar.set(xe_input_value)
					vents_page.vent_frame_list[i].ye_variable.Myvar.set(ye_input_value)
					vents_page.vent_frame_list[i].ti_variable.Myvar.set(ti_input_value)
					vents_page.vent_frame_list[i].tf_variable.Myvar.set(tf_input_value)
			if name_input_value=="#presence_of_trees":
				if input_value =="no":
					forest_page.no_Radio.select()
					forest_page.command_for_no()
				elif input_value in ["everywhere","region"]:
					if input_value =="everywhere":
						forest_page.everywhere_Radio.select()
						forest_page.command_for_everywhere()
					else:
						forest_page.region_Radio.select()
						forest_page.command_for_region()
						nb_regions_value = lines[lines.index(line)+4].split()[1]
						nb_regions_difference = int(nb_regions_value)-forest_page.regions_frame.nb_vents.get()
						if nb_regions_difference>0:
							for j in range(nb_regions_difference):
								forest_page.regions_frame.command_button()
						elif nb_regions_difference<0:
							for j in range(-nb_regions_difference):
								forest_page.regions_frame.vent_frame_list[-1].command_rm_button(forest_page.regions_frame)
						nb_line = lines.index(line)+5
						for j in range(forest_page.regions_frame.nb_vents.get()):
							nb_vertex_input_value = int(lines[nb_line].split()[1])
							nb_vertex_difference = nb_vertex_input_value - forest_page.regions_frame.vent_frame_list[j].Q_variable.nb_vents.get()
							if nb_vertex_difference>0:
								for k in range(nb_vertex_difference):
									forest_page.regions_frame.vent_frame_list[j].Q_variable.command_button()
							if nb_vertex_difference<0:
								for k in range(-nb_vertex_difference):
									forest_page.regions_frame.vent_frame_list[j].Q_variable.vent_frame_list[-1].command_rm_button(forest_page.regions_frame.vent_frame_list[j].Q_variable)
							nb_line+=2*nb_vertex_input_value+1
						nb_line = lines.index(line)+5
						for i in range(int(forest_page.regions_frame.nb_vents.get())):
							nb_line += 1
							for j in range(int(forest_page.regions_frame.vent_frame_list[i].Q_variable.nb_vents.get())):
								xe_input_value = lines[nb_line].split()[1]
								nb_line += 1
								ye_input_value = lines[nb_line].split()[1]
								nb_line += 1
								forest_page.regions_frame.vent_frame_list[i].Q_variable.vent_frame_list[j].xe_vertex_scale.Myvar.set(xe_input_value)
								forest_page.regions_frame.vent_frame_list[i].Q_variable.vent_frame_list[j].ye_vertex_scale.Myvar.set(ye_input_value)
					d_input_value = lines[lines.index(line)+1].split()[1]
					M_input_value = lines[lines.index(line)+2].split()[1]
					forest_page.d_variable.Myvar.set(d_input_value)
					forest_page.M_variable.Myvar.set(M_input_value)
					tree_packing_value = lines[lines.index(line)+3].split()[1]
					if tree_packing_value == "square":
						forest_page.square_Radio.select()
					elif tree_packing_value == "hexagonal":
						forest_page.hexa_Radio.select()
			if name_input_value=="#final_shape":
				if input_value == "none":
					simulation_page.filename_Var.set("none")
					simulation_page.shape_filename="none"
				else:
					try:
						with open(input_value,"r"): pass
					except:
						if messagebox.askyesno(title="Warning", message="Shapefile not found. Update file ?", parent=self.parent):
							simulation_page.open_shape_file()
						else:
							simulation_page.filename_Var.set("none")
							simulation_page.shape_filename="none"
					else:
						simulation_page.filename_Var.set(input_value.split("/")[-1])
						simulation_page.shape_filename=input_value
		self.parent.p3.scrollable_canvas.canvas.xview_moveto(0)
		self.parent.p3.scrollable_canvas.canvas.yview_moveto(0)
		self.parent.p4.scrollable_canvas.canvas.xview_moveto(0)
		self.parent.p4.scrollable_canvas.canvas.yview_moveto(0)
		print("end of load file")
	
	def exit_command(self):
			os.system("pkill lavaview")
			print("bye bye")
			plt.close('all')
			self.parent.parent.destroy()
			try:
				shutil.rmtree('tmp')
			except:
				pass
