///
/// This file is part of Shalava.
///
/// Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
///
/// Shalava is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Shalava is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Shalava; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;


int main (int argc, char **argv) {
  environment rheolef(argc,argv);
  if (argc == 1) {
    derr << "usage: " << argv[0] << " mesh approx[P1] nb_smooth[5] DEM_name[Hawaii]" << endl;
    exit (0);
  }
  geo omega (argv[1]);
  string approx     = (argc > 2)  ?      argv[2]   : "P1";
  size_t nb_smooth  = (argc > 3)  ? atoi(argv[3])  : 5;               // nb of smooth loop iteration
  string DEM_name   = (argc > 4)  ?      argv[4]   : "DEM_hawaii_2m.field";
  space Xh  (omega, approx);
  
  field f_topo;
  idiststream in (DEM_name);
  in >> f_topo;
  field fh = interpolate(Xh,f_topo);
  
  //odiststream fh_h ( "fh.field" );
  //fh_h << catchmark("fh") << fh;
  //fh_h.close();

  space Xh0  (omega, "P0");
  trial u0(Xh0);
  test v0(Xh0);
  form m0 = integrate(u0*v0);
  solver m0_fact (m0.uu());

  form_option_type fopt;
  fopt.set_family(quadrature_option_type::gauss_lobatto );
  fopt.set_order(1);
  fopt.lump = true;

  trial u1(Xh);
  test v1(Xh);
  form m1 = integrate(u1*v1,fopt);
  solver m1_fact (m1.uu());
  field fh_smooth0 (Xh0,0.);
  field fh_smooth1 = fh;
  for (size_t s=1; s <= nb_smooth; s++){
  field fh_sm0 = integrate(fh_smooth1*v0);
  fh_smooth0.set_u() = m0_fact.solve(fh_sm0.u());
  field fh_sm1 = integrate(fh_smooth0*v1);
  fh_smooth1.set_u() = m1_fact.solve(fh_sm1.u());
  }
  dout << fh_smooth1;
}
