#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
PKGDATADIR="."
PKGLIBDIR="."
from tkinter import *
from tkinter import ttk
import Pmw
from tkinter import messagebox
import os
from time import sleep
from threading import Thread
import utm
import json
import kml2geojson

import importlib.machinery
generate_macro_for_vtk = importlib.machinery.SourceFileLoader('generate_macro_for_vtk',PKGDATADIR+'/generate_macro_for_vtk.py').load_module()
vertical_canvas = importlib.machinery.SourceFileLoader('vertical_canvas',PKGDATADIR+'/vertical_canvas.py').load_module()
lavaview_variable = importlib.machinery.SourceFileLoader('lavaview_variable',PKGDATADIR+'/lavaview_variable.py').load_module()
hot_spot_Frame = importlib.machinery.SourceFileLoader('hot_spot_Frame',PKGDATADIR+'/hot_spot_Frame.py').load_module()
from vertical_canvas import *
from lavaview_variable import *
from hot_spot_Frame import *

class page5():
	"""
	A class to create the page 5 about the simulation
	"""
	def __init__(self, parent, mytitle, *args, **kw):
		self.parent=parent
		f = parent.n.add(mytitle)
		parent.n.tab(mytitle).configure(background=dark_grey,fg="white",state="disabled")
		self.scrollable_canvas  = VerticalScrolledFrame(f)
		self.scrollable_canvas.pack(fill = 'both', expand = 1)
		parent.n.tab(mytitle).bind('<1>',self.reset_the_view,add="+") #reset the view after click on tabs
		main_window = self.scrollable_canvas.interior
		Label(main_window,text="Simulation", bg=dark_grey, fg="white", font=("Helvetica", 16, "bold")).pack(padx = 5, pady = 5)

		# load shape file
		self.load_shape_Frame = Frame(main_window,bg=light_grey, highlightthickness=2)
		self.shape_vertex_list = []
		self.load_shape_Frame.pack(padx = 5, pady = 5)
		Label(self.load_shape_Frame, text="Load shape file", bg=light_grey, fg="white", font=("Helvetica", 12, "bold")).grid(row=0, column=0, padx = 5, pady = 5)
		self.load_file = PhotoImage(file=PKGDATADIR+'/load_file_icone_36px.png')
		self.load_shape_button = Button(self.load_shape_Frame, text="Load shape file", image=self.load_file, borderwidth=0, highlightthickness=0, command=self.open_shape_file)
		self.shape_filename = "none"
		self.filename_Var = StringVar()
		self.filename_Var.set("none")
		self.variable_filename_entry = Entry(self.load_shape_Frame,textvariable=self.filename_Var, width=10, font=("Helvetica", 16),state="disabled")
		self.load_shape_button.grid(row=0, column=2, padx = (0,5), pady = 5)
		self.variable_filename_entry.grid(row=0, column=1, padx = (5,0), pady = 5)

		Frame_for_nb_proc = Frame(main_window,bg=dark_grey)
		Frame_for_nb_proc.pack(padx = 5, pady = 5)
		self.nb_proc = Lavaview_variable(Frame_for_nb_proc,"nb_cores", "number of cores for parallel computing", 1,0,0,dark_grey)
		how_many_cores = Button(Frame_for_nb_proc,text="?",command=self.CPU_info)
		how_many_cores.grid(row=0,column=3, padx = 5, pady = 5)
		CPU_balloon = Pmw.Balloon(Frame_for_nb_proc)
		CPU_balloon.bind(how_many_cores, "How many cores on my PC ?")
		self.simulation_status_label = Label(main_window, bg=dark_grey, fg="white", font=("Helvetica", 16, "bold"))
		self.time_label = Label(main_window, bg=dark_grey, fg="white", font=("Helvetica", 12, "bold"))
		self.vtk_label = Label(main_window, bg=dark_grey, fg="white", font=("Helvetica", 12, "bold"))
		run_simulation_Button = Button(main_window, text = 'Run simulation', image=parent.parent.start_simulation_icone,background=dark_grey, borderwidth=0, highlightthickness=0, command = self.run_simulation_thread)
		run_simulation_Button.pack(padx = 5, pady = 5)
		self.stop_simulation_button = Button(main_window,text="Stop simulation",command=self.stop_simulation)
		Frame_for_visu = Frame(main_window,bg=dark_grey)
		Frame_for_visu.pack(padx = 5, pady = 5)
		generate_vtk_Button = ttk.Button(Frame_for_visu, text = 'Generate vtk', command = self.generate_vtk_thread)
		generate_vtk_Button.grid(row=0,column=0, padx = 5, pady = 25)
		view_in_paraview_Button = ttk.Button(Frame_for_visu, text = 'View simulation', command = self.view_in_vtk_thread)
		view_in_paraview_Button.grid(row=0,column=1, padx = 5, pady = 25)
		self.iteration_for_xyz = Lavaview_variable(Frame_for_visu,"Iteration", "choose iteration for xyz file", 0,1,0,dark_grey)
		generate_xyz_Button = ttk.Button(Frame_for_visu, text = 'Generate xyz file', command = self.generate_xyz_thread)
		generate_xyz_Button.grid(row=1,column=2, padx = 5, pady = 25)
		Frame_for_button=Frame(main_window,bg=dark_grey)
		Frame_for_button.pack(padx = 5, pady = 5)
		previous_from_5_to_4 = ttk.Button(Frame_for_button, text = 'Previous', command = lambda:n.previouspage())
	
	def open_shape_file(self):
		filename=tkinter.filedialog.askopenfilename(title="Open shapefile", filetypes=[('geojson files','.geojson'),('kml files','.kml')], parent=self.parent)
		if not filename: # askopenfilename return `false` if dialog closed with "cancel".
			return
		self.shape_filename = filename
		self.filename_Var.set(filename.split("/")[-1])

	def write_shape_vertex_list(self):
		os.makedirs('tmp', exist_ok=True)
		shape_filename = self.shape_filename
		short_filename = self.filename_Var.get()
		if short_filename.split(".")[-1]=="kml":
			kml2geojson.main.convert(shape_filename,output_dir="tmp")
			shape_filename = "tmp/"+".".join(short_filename.split(".")[0:-1])+".geojson"
		with open(shape_filename) as f:
			data = json.load(f)
		for feature in data['features']:
			my_file = open("final_shape0.tmp", "w") #erase
			list_of_coordinates = feature['geometry']['coordinates']
			try:
				float(list_of_coordinates[0][0])
			#if len(list_of_coordinates)==1: #from GeoJson,list_of_coordinates=[[coordinate]] 1 hook too much
			except:
				list_of_coordinates = list_of_coordinates[0]
			del list_of_coordinates[-1]
			for vertex in list_of_coordinates:
					utm_convert = utm.from_latlon(vertex[1],vertex[0])
					my_file.write(str(utm_convert[0])+" "+str(utm_convert[1])+"\n")
			my_file.close()
			os.system("head -n -1 final_shape0.tmp > tmp/final_shape.tmp")
			os.system("rm final_shape0.tmp")
			break

	def reset_the_view(self, event):
		self.scrollable_canvas.canvas.xview_moveto(0)
		self.scrollable_canvas.canvas.yview_moveto(0)

	def CPU_info(self):
			nb_cores=os.popen("cat /proc/cpuinfo | grep -i \"^processor\" | wc -l").read().rstrip()
			messagebox.showinfo("Number of cores on my machine", "you have "+nb_cores+" cores on your machine",parent=self.parent)
	
	def simulation_time(self):
		sleep(10)
		self.time_label.config(text="Iteration : 0, Simulation time: 0, Real time: 0")
		self.time_label.pack(padx = 5, pady = 5)
		self.stop_simulation_button.pack(padx = 5, pady = 5)
		nb_iter = 0
		simulation_time = 0
		real_time = 0
		nb_lines=int(os.popen("top -b -n 1 | grep lavaview | wc -l").read().rstrip())
		while nb_lines>0:
			try:
				last_line = os.popen("tail -n 1 data.log").read().rstrip()
				nb_iter = last_line.split()[0]
				simulation_time = last_line.split()[1]
				real_time = last_line.split()[2]
			except:
				pass
			self.time_label.config(text="Iteration : "+nb_iter+", Simulation time: "+simulation_time+", Real time: "+real_time)
			sleep(1)
			nb_lines=int(os.popen("top -b -n 1 | grep lavaview | wc -l").read().rstrip())
		self.simulation_status_label.config(text="Simulation done")
	
	def write_file(self,my_file):
		my_file.write("#DEM_filename "+self.parent.p1.DEM_filename+ "\n")
		my_file.write("#UTM_zone "+self.parent.p1.utm_zone.Myvar.get()+ "\n")
		my_file.write("#south "+self.parent.p1.south_variable.Myvar.get()+ "\n")
		my_file.write("#est "+self.parent.p1.est_variable.Myvar.get()+ "\n") 
		my_file.write("#north "+self.parent.p1.north_variable.Myvar.get()+ "\n")
		my_file.write("#west "+self.parent.p1.west_variable.Myvar.get()+ "\n")
		my_file.write("#reso "+self.parent.p1.reso_variable.Myvar.get()+ "\n")
		my_file.write("#rho "+self.parent.p2.rho_variable.Myvar.get()+ "\n")
		my_file.write("#mu0 "+self.parent.p2.mu_variable.Myvar.get()+ "\n")
		my_file.write("#tau "+self.parent.p2.tau_variable.Myvar.get()+ "\n")
		my_file.write("#nb_vents %d"%self.parent.p3.hot_spot_Frame.nb_vents.get() + "\n")
		for i in range(int(self.parent.p3.hot_spot_Frame.nb_vents.get())):
			my_file.write("#flow_rate_filename_%d"%(i+1)+" "+self.parent.p3.hot_spot_Frame.vent_frame_list[i].flow_rate_filename+"\n")
			my_file.write("#Q_%d"%(i+1)+" "+self.parent.p3.hot_spot_Frame.vent_frame_list[i].Q_variable.Myvar.get()+"\n")
			my_file.write("#R_%d"%(i+1)+" "+self.parent.p3.hot_spot_Frame.vent_frame_list[i].R_variable.Myvar.get()+"\n")
			my_file.write("#xe_%d"%(i+1)+" "+self.parent.p3.hot_spot_Frame.vent_frame_list[i].xe_variable.Myvar.get()+"\n")
			my_file.write("#ye_%d"%(i+1)+" "+self.parent.p3.hot_spot_Frame.vent_frame_list[i].ye_variable.Myvar.get()+"\n")
			my_file.write("#ti_%d"%(i+1)+" "+self.parent.p3.hot_spot_Frame.vent_frame_list[i].ti_variable.Myvar.get()+"\n")
			my_file.write("#tf_%d"%(i+1)+" "+self.parent.p3.hot_spot_Frame.vent_frame_list[i].tf_variable.Myvar.get()+"\n")
		if int(self.parent.p4.forest_characteristics_Frame.varGr.get())==1:
			my_file.write("#presence_of_trees no\n")
		elif int(self.parent.p4.forest_characteristics_Frame.varGr.get())==2:
			my_file.write("#presence_of_trees everywhere\n")
		else:
			my_file.write("#presence_of_trees region\n")
		if int(self.parent.p4.forest_characteristics_Frame.varGr.get())>1:
			my_file.write("#d "+self.parent.p4.forest_characteristics_Frame.d_variable.Myvar.get()+ "\n")
			my_file.write("#M "+self.parent.p4.forest_characteristics_Frame.M_variable.Myvar.get()+ "\n")
			if int(self.parent.p4.forest_characteristics_Frame.arrangement_var.get())==1:
				my_file.write("#arrangement_of_trees square\n")
			else:
				my_file.write("#arrangement_of_trees hexagonal")
			if int(self.parent.p4.forest_characteristics_Frame.varGr.get())==3:
				my_file.write("#nb_regions %d"%self.parent.p4.forest_characteristics_Frame.regions_frame.nb_vents.get() + "\n")
				for i in range(int(self.parent.p4.forest_characteristics_Frame.regions_frame.nb_vents.get())):
					my_file.write("#nb_vertex_%d"%(i+1)+" "+str(self.parent.p4.forest_characteristics_Frame.regions_frame.vent_frame_list[i].Q_variable.nb_vents.get())+"\n")
					for j in range(int(self.parent.p4.forest_characteristics_Frame.regions_frame.vent_frame_list[i].Q_variable.nb_vents.get())):
						my_file.write("#vertex_x_{}_{}".format((i+1),(j+1))+" "+str(self.parent.p4.forest_characteristics_Frame.regions_frame.vent_frame_list[i].Q_variable.vent_frame_list[j].xe_vertex_scale.Myscalevar.get())+"\n")
						my_file.write("#vertex_y_{}_{}".format((i+1),(j+1))+" "+str(self.parent.p4.forest_characteristics_Frame.regions_frame.vent_frame_list[i].Q_variable.vent_frame_list[j].ye_vertex_scale.Myscalevar.get())+"\n")
		my_file.write("#final_shape "+self.shape_filename)
		my_file.close()

	def save_input(self):
		my_file = open("tmp/input.tmp", "w") #erase
		self.write_file(my_file)
	
	def run_simulation(self):
		if self.filename_Var.get()!="none":
			self.write_shape_vertex_list()
		error=0
		for vent in self.parent.p3.hot_spot_Frame.vent_frame_list:
			if int(vent.varQ.get())==2 and vent.flow_rate_filename=="none":
				messagebox.showerror(title="Error with flow rate (vent "+str(vent.vent_nb_variable.get())+")", message="Please choose a flow rate file or select constant rate before to start simulation!", parent=self.parent)
				error=1
				break
		if error==0:
			self.simulation_status_label.config(text="Simulation in progress...")
			self.simulation_status_label.pack(padx = 5, pady = 5)
			#os.system("pkill branch")
			os.system("pkill lavaview")
			os.system("rm output-*.branch.gz lava-*.vtk")
			#os.system("rm *gz lava-*.vtk *branch *bb data.log")
			os.makedirs('tmp', exist_ok=True)
			self.save_input()
			os.system("mpirun -np "+str(int(self.nb_proc.Myvar.get()))+" "+PKGLIBDIR+"/lavaview grille.geo P1 10000000 1 < tmp/input.tmp &")
			self.simulation_time()
	
	def run_simulation_thread(self):
		my_thread=Thread(target=self.run_simulation)
		my_thread.start()
	
	def stop_simulation(self):
		os.system("pkill lavaview")
	
	def generate_vtk(self):
		self.vtk_label.pack(padx = 5, pady = 5)
		last_vtk=0
		sleep(0.5)
		nb_lines=int(os.popen("top -b -n 1 | grep lavaview | wc -l").read().rstrip()) #nb of lavaview id
		if (self.filename_Var.get()=="none"):
			bool_shape = 0
		else:
			bool_shape = 1
		if int(self.parent.p4.forest_characteristics_Frame.varGr.get())==3:
			bool_region = 1
		else:
			bool_region = 0
		while nb_lines>0:
			bool_vtk = int(os.popen("ls output-"+str(last_vtk+1)+".branch.gz | wc -l").read().rstrip())
			if bool_shape:
				bool_shapefile = int(os.popen("ls id_shape.branch.gz | wc -l").read().rstrip())
				if bool_shapefile:
					os.system("branch -paraview id_shape.branch")
					bool_shape = 0
			if bool_region:
				bool_forest_file = int(os.popen("ls id_forest.branch.gz  | wc -l").read().rstrip())
				if bool_forest_file:
					os.system("branch -paraview id_forest.branch")
					bool_region = 0
			if bool_vtk:
				os.system("branch -paraview output-"+str(last_vtk)+".branch")
				#os.system("rm output-"+str(last_vtk)+".branch.gz")
				os.system("mv output-"+str(last_vtk)+"-0.vtk lava-"+str(last_vtk)+".vtk")
				"""vtk_counter = str(last_vtk)
				if last_vtk<10:
					vtk_counter = "00"+str(last_vtk)
				elif last_vtk<100:
					vtk_counter = "0"+str(last_vtk)
				os.system("rm grille-"+vtk_counter+".geo.gz")"""
				self.vtk_label.config(text="\'lava-"+str(last_vtk)+".vtk\' created")
				last_vtk+= 1
			nb_lines=int(os.popen("top -b -n 1 | grep lavaview | wc -l").read().rstrip()) #nb of lavaview id
		while 1:
			bool_vtk = int(os.popen("ls output-"+str(last_vtk+1)+".branch.gz | wc -l").read().rstrip())
			if bool_vtk:
				os.system("branch -paraview output-"+str(last_vtk)+".branch")
				#os.system("rm output-"+str(last_vtk)+".branch.gz")
				os.system("mv output-"+str(last_vtk)+"-0.vtk lava-"+str(last_vtk)+".vtk")
				self.vtk_label.config(text="\'lava-"+str(last_vtk)+".vtk\' created")
				last_vtk+= 1
			else: 
				break
		self.vtk_label.config(text="end vtk creation")

	def generate_xyz(self):
		os.makedirs('DEM_generated', exist_ok=True)
		xe = self.parent.p3.hot_spot_Frame.vent_frame_list[0].xe_variable.Myvar.get()
		ye = self.parent.p3.hot_spot_Frame.vent_frame_list[0].ye_variable.Myvar.get()
		south = self.parent.p1.south_variable.Myvar.get()
		west = self.parent.p1.west_variable.Myvar.get()
		reso = self.parent.p1.reso_variable.Myvar.get()
		iteration = self.iteration_for_xyz.Myvar.get()
		os.system(PKGLIBDIR+"/output2xyz grille.geo P1 "+xe+" "+ye+" "+south+" "+west+" "+reso+" "+iteration+" &")

	
	def generate_vtk_thread(self):
		my_thread=Thread(target=self.generate_vtk)   #définit la fonction a executer en arrière-plan
		my_thread.start()    #lance la fonction, sans faire freezer la fenêtre

	def generate_xyz_thread(self):
		my_thread=Thread(target=self.generate_xyz)   #définit la fonction a executer en arrière-plan
		my_thread.start()    #lance la fonction, sans faire freezer la fenêtre
	
	def view_in_vtk_thread(self):
		os.makedirs('tmp', exist_ok=True)
		#if (self.filename_Var.get()=="none"):
		my_thread=Thread(target=generate_macro_for_vtk.write_vtk_macro(\
			self.filename_Var.get(),\
			int(self.parent.p4.forest_characteristics_Frame.varGr.get())).view_in_vtk)
		"""else:
			my_thread=Thread(target=generate_macro_for_vtk_with_shape.view_in_vtk)"""
		my_thread.start()
