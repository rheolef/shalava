#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
PKGDATADIR="."
import Pmw
from tkinter import *
import matplotlib.pyplot as plt
import os
import shutil

import importlib.machinery
page1_simulation = importlib.machinery.SourceFileLoader('page1_simulation',PKGDATADIR+'/page1_simulation.py').load_module()
page2_simulation = importlib.machinery.SourceFileLoader('page2_simulation',PKGDATADIR+'/page2_simulation.py').load_module()
page3_simulation = importlib.machinery.SourceFileLoader('page3_simulation',PKGDATADIR+'/page3_simulation.py').load_module()
page4_simulation = importlib.machinery.SourceFileLoader('page4_simulation',PKGDATADIR+'/page4_simulation.py').load_module()
page5_simulation = importlib.machinery.SourceFileLoader('page5_simulation',PKGDATADIR+'/page5_simulation.py').load_module()
mymenu = importlib.machinery.SourceFileLoader('mymenu',PKGDATADIR+'/mymenu.py').load_module()

class simulation_Toplevel(Toplevel):
	def __init__(self, parent, mytitle, *args, **kw):
		Toplevel.__init__(self, parent, *args, **kw)
		self.parent=parent
		self.title(mytitle)
		self.configure(bg="#171717")
		self.w = int(self.winfo_screenwidth())
		self.h = int(self.winfo_screenheight())
		#self.overrideredirect(0)
		self.geometry("%dx%d+0+0" % ( self.w,  self.h))
		self.n = Pmw.NoteBook(self,pagemargin=0)
		Pmw.Color.changecolor(self.n.component('hull'), background='#171717')
		self.n.pack(fill = 'both', expand = 1, padx = 5, pady = 5)
		#creation of pages
		self.p1 = page1_simulation.page1(parent=self,mytitle='Domain')
		self.p2 = page2_simulation.page2(parent=self,mytitle='Fluid')
		self.p3 = page3_simulation.page3(parent=self,mytitle='Vents')
		self.p4 = page4_simulation.page4(parent=self,mytitle='Forest')
		self.p5 = page5_simulation.page5(parent=self,mytitle='Simulation')
		#creation of menu
		self.menu=mymenu.Mymenu(self)
		self.protocol("WM_DELETE_WINDOW", self.quit_command)
	def quit_command(self):
		os.system("pkill lavaview")
		print("bye bye")
		plt.close('all')
		self.parent.destroy()
		try:
			shutil.rmtree('tmp')
		except:
			pass

