#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
PKGDATADIR="."
import folium
import utm
import webbrowser
import base64
import io
import kml2geojson
import os


class mymap():
	def __init__(self,utm_zone, xmin,xmax,ymin,ymax,south,est,north,west,vents_list,shape_filename, region):
		os.makedirs('tmp', exist_ok=True)
		self.ZoneLetter=utm_zone[-1]
		self.ZoneNumber=int(utm_zone[0:-1])
		self.BotLeftCoords = utm.to_latlon(xmin,ymin,self.ZoneNumber,self.ZoneLetter)
		self.TopRightCoords = utm.to_latlon(xmax,ymax,self.ZoneNumber,self.ZoneLetter)
		self.xe = float(vents_list[0].xe_variable.Myvar.get())
		self.ye = float(vents_list[0].ye_variable.Myvar.get())
		self.south = float(south.Myvar.get())
		self.est = float(est.Myvar.get())
		self.north = float(north.Myvar.get())
		self.west = float(west.Myvar.get())
		short_filename = shape_filename.split("/")[-1]
		if shape_filename.split(".")[-1]=="kml":
			kml2geojson.main.convert(shape_filename,output_dir="tmp")
			shape_filename = "tmp/"+".".join(short_filename.split(".")[0:-1])+".geojson"
		self.shape_filename = shape_filename
		self.region_list = region.regions_frame.vent_frame_list
		center_coords = utm.to_latlon(self.xe, self.ye, self.ZoneNumber, self.ZoneLetter)
		center = [center_coords[0], center_coords[1]]
		#Create the map
		self.my_map = folium.Map(location = center, zoom_start = 13, tiles='Stamen Terrain')
		#Add markers to the map
		#custom icon
		for nb, vent in enumerate(vents_list):
			vent_coords = utm.to_latlon(float(vent.xe_variable.Myvar.get()),float(vent.ye_variable.Myvar.get()), self.ZoneNumber, self.ZoneLetter)
			vent = [vent_coords[0], vent_coords[1]]
			icon_path = PKGDATADIR +"/icon_eruption.png"
			icon = folium.features.CustomIcon(icon_image=icon_path ,icon_size=(30,30))
			folium.Marker(vent, icon=icon, popup = 'vent '+str(1+nb)).add_to(self.my_map)
		self.create_geojson_retangle()
		if int(region.varGr.get())==3:
			self.create_forest_region()
		self.my_map.save('tmp/my_map.html')
		webbrowser.open('tmp/my_map.html',new=0, autoraise=True)
	def create_forest_region(self):
		print( 'nb of regions = '+str(len(self.region_list)))
		for nb, region in enumerate(self.region_list):
			p=[]
			vertex_list = region.Q_variable.vent_frame_list
			for vertex in vertex_list:
				xe = vertex.xe_vertex_scale.Myscalevar.get()
				ye = vertex.ye_vertex_scale.Myscalevar.get()
				latlon = utm.to_latlon(xe,ye,self.ZoneNumber,self.ZoneLetter)
				q = [latlon[1],latlon[0]]
				p.append(str(q))
			p.append(p[0])
			my_file = open("tmp/forest_region.geojson", "w") # erase file
			my_file.write("{\"type\": \"FeatureCollection\",\"features\": [{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[")
			my_file.write(','.join(p)+"]]}}]}")
			my_file.close()
			mygeojson = "tmp/forest_region.geojson"
			gj = folium.GeoJson(mygeojson,style_function=lambda x :{'fillOpacity': 0.1,'fillColor':'green', 'color':'green','opacity': 0.7,'weight': 2})
			gj.add_child(folium.Popup('forest region '+str(nb+1)))
			gj.add_to(self.my_map)
	def create_geojson_retangle(self):
		# DEM
		x1 = self.BotLeftCoords[0]
		y1 = self.BotLeftCoords[1]
		x0 = self.TopRightCoords[0]
		y0 = self.TopRightCoords[1]
		p1 = [y0,x0]
		p2 = [y0,x1]
		p3 = [y1,x1]
		p4 = [y1,x0]
		# Simulation domain
		xe = self.xe
		ye = self.ye
		south = self.south
		est = self.est
		north = self.north
		west = self.west
		BotLeftCoords_simulation = utm.to_latlon(xe-west,ye-south,self.ZoneNumber,self.ZoneLetter)
		TopRightCoords_simulation = utm.to_latlon(xe+est,ye+north,self.ZoneNumber,self.ZoneLetter)
		xs1 = BotLeftCoords_simulation[0]
		ys1 = BotLeftCoords_simulation[1]
		xs0 = TopRightCoords_simulation[0]
		ys0 = TopRightCoords_simulation[1]
		q1 = [ys0,xs0]
		q2 = [ys0,xs1]
		q3 = [ys1,xs1]
		q4 = [ys1,xs0]
		p=[str(p1),str(p2),str(p3),str(p4),str(p1)]
		q=[str(q1),str(q2),str(q3),str(q4),str(q1)]
		"""my_file = open("rectangle.geojson", "w") # erase file
		my_file.write("{\"type\": \"FeatureCollection\",\"features\": [{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[")
		#my_file.write("{\"type\": \"FeatureCollection\",\"features\": [{\"type\": \"Feature\",\"properties\": {\"stroke\": \"#000000\",\"stroke-width\": 2,\"stroke-opacity\": 1,\"fill\": \"#d16e6e\",\"fill-opacity\": 0.3},\"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[")
		my_file.write(','.join(p)+"]]}},\n")
		my_file.write("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[")
		my_file.write(','.join(q)+"]]}}]}")
		my_file.close()"""
		my_file = open("tmp/rectangle.geojson", "w") # erase file
		my_file.write("{\"type\": \"FeatureCollection\",\"features\": [{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[")
		my_file.write(','.join(p)+"]]}}]}")
		my_file.close()
		my_file = open("tmp/rectangle_inside.geojson", "w") # erase file
		my_file.write("{\"type\": \"FeatureCollection\",\"features\": [{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[")
		my_file.write(','.join(q)+"]]}}]}")
		my_file.close()
		mygeojson = "tmp/rectangle.geojson"
		gj = folium.GeoJson(mygeojson,style_function=lambda x :{'fillOpacity': 0.1,'fillColor':'black', 'color':'black','opacity': 0.7,'weight': 3})
		gj.add_child(folium.Popup('outline DEM'))
		gj.add_to(self.my_map)
		mygeojson2 = "tmp/rectangle_inside.geojson"
		gj2 = folium.GeoJson(mygeojson2,style_function=lambda x :{'fillOpacity': 0.1,'fillColor':'blue', 'color':'blue','opacity': 0.7,'weight': 2})
		gj2.add_child(folium.Popup('simulation domain'))
		gj2.add_to(self.my_map)
		if self.shape_filename!="none":
			mygeojson3 = self.shape_filename #format GeoJson
			gj3 = folium.GeoJson(mygeojson3,style_function=lambda x :{'fillOpacity': 0.1,'fillColor':'red', 'color':'red','opacity': 0.7,'weight': 2})
			gj3.add_child(folium.Popup('Lava deposit shape'))
			gj3.add_to(self.my_map)
	#Display the map
