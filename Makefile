#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
MAIN_CC = \
	lavaview.cc              \
	mkgeo_grid_2d_var.cc     \
	smooth_DEM.cc            \
	output2xyz.cc            \

CC_INC = \
	lavaview.h	\
	lavaview1.icc	\
	lavaview2.icc	\
	invert_h.icc	\
	variable_flow_rate.icc	\
	input.h \
	input.icc \
	print_input.icc \
	read_input.icc \
	read_shape_input.icc \
	mu.icc \
	hc_scalar.icc \
	penalisation.icc \
	eta.icc \
	beta.icc \
	write_output_xyz.icc \

MAIN_PY = \
	shalava	\

PY_INC = \
	mymenu.py \
	lavaview.py \
	lavaview_variable.py \
	page1_simulation.py \
	page2_simulation.py \
	page3_simulation.py \
	page4_simulation.py \
	page5_simulation.py \
	vertical_canvas.py \
	hot_spot_Frame.py \
	macro_for_vtk.py \
	generate_macro_for_vtk.py \
	simulation_setting_window.py \
	pymap.py \

PY_PNG = \
	logo_shalava.png \
	icon_eruption.png \
	load_buttom_100px.png \
	load_file_icone_36px.png \
	cross.gif \
	start_simulation_buttom_100px.png \
	utm-zones.png \

MAIN_TEX = 			\
	doc_shalava.tex		\

TEX_INC = \
	biblio.bib

TEX_PNG = \
	icon_eruption_doc.png \
	domain_tab.png	\
	osm_domain.png	\
	paraview.png	\
	shalava_home.png	\
	tab_fluid.png	\
	vents_tab.png	\

XYZ_SH_TOMOVE = \
	simulation_domain_kilauea_2m.sh \
	simulation_domain_reunion_5m.sh \
	simulation_domain_fournaise_5m.sh \
	simulation_domain_etna2001_10m.sh \

SH_SRC = \
	xyz2geofield.sh \

        
DISTFILES = 		         \
	Makefile                 \
	README	                 \
	INSTALL.html             \
	AUTHORS			 \
	COPYING			 \
	configure		 \
	$(MAIN_CC)		 \
	$(MAIN_PY)		 \
	$(MAIN_TEX)		 \
	$(CC_INC)		 \
	$(PY_INC)		 \
	$(TEX_INC)		 \
	$(PY_PNG)		 \
	$(TEX_PNG)		 \
	$(SH_SRC)		 \
	clean.sh 		 \

include $(shell rheolef-config --libdir)/rheolef/rheolef.mk
CXXFLAGS  = $(INCLUDES_RHEOLEF) `mpic++ --showme:compile`
LDLIBS    = $(LIBS_RHEOLEF)

ALL = $(MAIN_CC:.cc=) $(MAIN_TEX:.tex=.pdf)
all: $(ALL)
clean:
	rm -f $(ALL)

# config.mk: cree par configure
# DESTDIR    = $(HOME)/sys-shalava
# PKGLIBDIR  = $(DESTDIR)/lib/shalava
# PKGDATADIR = $(DESTDIR)/share/shalava
include config.mk
WEBDIR = $(HOME)/public_html/shalava
config.mk:
	@echo "ERROR: please run the configure script before make"; exit 1

install: $(ALL)
	mkdir -p $(DESTDIR)/bin
	mkdir -p $(PKGLIBDIR)
	mkdir -p $(PKGDATADIR)
	-rm -f $(PKGDATADIR)/* $(PKGDATADIR)/__pycache__/* $(PKGLIBDIR)/*
	-rmdir $(PKGDATADIR)/__pycache__
	cp shalava $(DESTDIR)/bin/shalava
	chmod a+x $(DESTDIR)/bin/shalava
	cp $(MAIN_CC:.cc=) $(PKGLIBDIR)/
	cp $(PY_INC) $(PY_PNG) $(SH_SRC) $(PKGDATADIR)/
	for x in $(DESTDIR)/bin/shalava 						\
	         $(PKGDATADIR)/page1_simulation.py					\
	         $(PKGDATADIR)/page3_simulation.py					\
	         $(PKGDATADIR)/page5_simulation.py					\
	         $(PKGDATADIR)/page5_simulation.py					\
	         $(PKGDATADIR)/hot_spot_Frame.py					\
	         $(PKGDATADIR)/simulation_setting_window.py				\
	         $(PKGDATADIR)/pymap.py 						\
	; do 									  	\
	  sed -i 's/PKGLIBDIR="."/PKGLIBDIR="$(subst /,\/,${PKGLIBDIR})"/g'    $$x;	\
	  sed -i 's/PKGDATADIR="."/PKGDATADIR="$(subst /,\/,${PKGDATADIR})"/g' $$x; 	\
	done
	cp $(MAIN_TEX:.tex=.pdf) $(PKGDATADIR)/shalava.pdf

web-install: $(ALL)
	cp INSTALL.html          $(WEBDIR)/index.html
	cp $(MAIN_TEX:.tex=.pdf) $(WEBDIR)/shalava.pdf
uninstall:
	rm -f $(DESTDIR)/bin/shalava
	rm -f $(PKGLIBDIR)/* $(PKGDATADIR)/*
	-rmdir $(PKGLIBDIR) $(PKGDATADIR)

%.pdf: %.tex
	pdflatex $*
	bibtex   $*
	pdflatex $*
	pdflatex $*

-include ${CVSMKROOT}/git.mk
