#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()
fh_smoothvtk = LegacyVTKReader(FileNames=['/home/bernabeu/lava_forest/shalava_fev2019/fh_smooth.vtk'])
renderView1 = GetActiveViewOrCreate('RenderView')
scalarLUT = GetColorTransferFunction('scalar')
scalarLUT.RGBPoints = [10.779187202453613, 0.231373, 0.298039, 0.752941, 11.1914381980896, 0.865003, 0.865003, 0.865003, 11.603689193725586, 0.705882, 0.0156863, 0.14902]
scalarLUT.ScalarRangeInitialized = 1.0
fh_smoothvtkDisplay = Show(fh_smoothvtk, renderView1)
fh_smoothvtkDisplay.ColorArrayName = ['POINTS', 'scalar']
fh_smoothvtkDisplay.LookupTable = scalarLUT
fh_smoothvtkDisplay.OSPRayScaleArray = 'scalar'
fh_smoothvtkDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
fh_smoothvtkDisplay.GlyphType = 'Arrow'
fh_smoothvtkDisplay.ScalarOpacityUnitDistance = 0.4929976581906386
renderView1.ResetCamera()
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [5.004600148182362, 5.009799864143133, 10000.0]
renderView1.CameraFocalPoint = [5.004600148182362, 5.009799864143133, 0.0]
fh_smoothvtkDisplay.SetScalarBarVisibility(renderView1, True)
scalarPWF = GetOpacityTransferFunction('scalar')
scalarPWF.Points = [10.779187202453613, 0.0, 0.5, 0.0, 11.603689193725586, 1.0, 0.5, 0.0]
scalarPWF.ScalarRangeInitialized = 1
warpByScalar1 = WarpByScalar(Input=fh_smoothvtk)
warpByScalar1.Scalars = ['POINTS', 'scalar']
warpByScalar1.ScaleFactor = 2.0
warpByScalar1Display = Show(warpByScalar1, renderView1)
warpByScalar1Display.ColorArrayName = ['POINTS', 'scalar']
warpByScalar1Display.LookupTable = scalarLUT
warpByScalar1Display.OSPRayScaleArray = 'scalar'
warpByScalar1Display.OSPRayScaleFunction = 'PiecewiseFunction'
warpByScalar1Display.GlyphType = 'Arrow'
warpByScalar1Display.ScalarOpacityUnitDistance = 0.4963511142733224
Hide(fh_smoothvtk, renderView1)
warpByScalar1Display.SetScalarBarVisibility(renderView1, True)
warpByScalar1Display.SetScalarBarVisibility(renderView1, False)
lava = LegacyVTKReader(FileNames=[])
animationScene1 = GetAnimationScene()
animationScene1.UpdateAnimationUsingDataTimeSteps()
timeLUT = GetColorTransferFunction('time')
timeLUT.RGBPoints = [0.1502791792154312, 0.231373, 0.298039, 0.752941, 0.15027993061884126, 0.865003, 0.865003, 0.865003, 0.15028068202225128, 0.705882, 0.0156863, 0.14902]
timeLUT.ScalarRangeInitialized = 1.0
lavaDisplay = Show(lava, renderView1)
lavaDisplay.ColorArrayName = ['POINTS', 'time']
lavaDisplay.LookupTable = timeLUT
lavaDisplay.OSPRayScaleArray = 'time'
lavaDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
lavaDisplay.GlyphType = 'Arrow'
lavaDisplay.ScalarOpacityUnitDistance = 1.601868418129527
lavaDisplay.SetScalarBarVisibility(renderView1, True)
timePWF = GetOpacityTransferFunction('time')
timePWF.Points = [0.1502791792154312, 0.0, 0.5, 0.0, 0.15028068202225128, 1.0, 0.5, 0.0]
timePWF.ScalarRangeInitialized = 1
warpByScalar2 = WarpByScalar(Input=lava)
warpByScalar2.Scalars = ['POINTS', 'time']
warpByScalar2.Scalars = ['POINTS', 'f+h']
warpByScalar2.ScaleFactor = 2.01
warpByScalar2Display = Show(warpByScalar2, renderView1)
warpByScalar2Display.ColorArrayName = ['POINTS', 'time']
warpByScalar2Display.LookupTable = timeLUT
warpByScalar2Display.OSPRayScaleArray = 'time'
warpByScalar2Display.OSPRayScaleFunction = 'PiecewiseFunction'
warpByScalar2Display.GlyphType = 'Arrow'
warpByScalar2Display.ScalarOpacityUnitDistance = 1.6128639586893658
Hide(lava, renderView1)
warpByScalar2Display.SetScalarBarVisibility(renderView1, True)
ColorBy(warpByScalar2Display, ('POINTS', 'h'))
warpByScalar2Display.RescaleTransferFunctionToDataRange(True)
warpByScalar2Display.SetScalarBarVisibility(renderView1, True)
hLUT = GetColorTransferFunction('h')
hLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.008083454333245754, 0.865003, 0.865003, 0.865003, 0.01616690866649151, 0.705882, 0.0156863, 0.14902]
hLUT.ScalarRangeInitialized = 1.0
hPWF = GetOpacityTransferFunction('h')
hPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.01616690866649151, 1.0, 0.5, 0.0]
hPWF.ScalarRangeInitialized = 1
hLUT.ApplyPreset('Black-Body Radiation', True)
hPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.0005148696945980191, 0.9671052694320679, 0.5, 0.0, 0.01616690866649151, 1.0, 0.5, 0.0]
hPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.00015446091128978878, 0.9868420958518982, 0.5, 0.0, 0.01616690866649151, 1.0, 0.5, 0.0]
hPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.0, 1.0, 0.5, 0.0, 0.01616690866649151, 1.0, 0.5, 0.0]
hLUT.EnableOpacityMapping = 1
hLUTColorBar = GetScalarBar(hLUT, renderView1)
hLUTColorBar.Title = 'h'
hLUTColorBar.ComponentTitle = ''
hLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
hLUTColorBar.TitleBold = 1
hLUTColorBar.TitleFontSize = 12
hLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
hLUTColorBar.LabelBold = 1
hLUTColorBar.LabelFontSize = 12
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [5.004600148182362, 5.009799864143133, 10000.0]
renderView1.CameraFocalPoint = [5.004600148182362, 5.009799864143133, 0.0]
renderView1.CameraParallelScale = 7.056925684789655
