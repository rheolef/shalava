#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
from tkinter import *

dark_grey = "#282827"
light_grey = "#484845"
super_light_grey = "#65655F"

class VerticalScrolledFrame(Frame):
    """A pure Tkinter scrollable frame that actually works!
    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling
    """
    def __init__(self, parent, *args, **kw):
        Frame.__init__(self, parent, *args, **kw)
        # create a canvas object and a vertical scrollbar for scrolling it
        vscrollbar = Scrollbar(self, orient=VERTICAL,bg=super_light_grey, troughcolor=light_grey)
        vscrollbar.pack(fill=Y, side=RIGHT, expand=FALSE)
        w = parent.winfo_screenheight()
        self.canvas = canvas = Canvas(self, bd=0, width=0,height=w-100,scrollregion=(0,0,500,500),highlightthickness=0,yscrollcommand=vscrollbar.set, bg="#282827")
        canvas.pack(side=LEFT, fill=BOTH, expand=TRUE)
        vscrollbar.config(command=canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = Frame(canvas,bg=dark_grey)
        interior_id = canvas.create_window(0,0, window=interior, anchor=NW)
        def _on_mousewheel_up(event):
               canvas.yview('scroll', -1, 'units')
        def _on_mousewheel_down(event):
                canvas.yview('scroll', 1, 'units')
        self.interior.bind_all('<4>', _on_mousewheel_up,add="+")
        self.interior.bind_all('<5>', _on_mousewheel_down,add="+")
        self.interior.bind_all('<Up>', _on_mousewheel_up,add="+")
        self.interior.bind_all('<Down>', _on_mousewheel_down,add="+")

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())
        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())
        canvas.bind('<Configure>', _configure_canvas)
