///
/// This file is part of Shalava.
///
/// Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
///
/// Shalava is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Shalava is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Shalava; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
Float F (const Float& z, const Float& h, const Float& xi, const Float& Dam1, const Float& Bi_star, const Float& Bi) { return Bi*cosh(z*sqrt(Dam1))+(z-h)*(xi-Bi_star*Dam1);}
Float F_prime (const Float& z, const Float& xi, const Float& Dam1, const Float& Bi_star, const Float& Bi) { return (Bi*sqrt(Dam1))*sinh(z*sqrt(Dam1))+xi-Bi_star*Dam1;}
Float hc_scalar (const Float& h, const Float& xi, const Float& Dam1, const Float& Bi_star, const Float& Bi) {
	if ( (h*(xi-Bi_star*Dam1) <= Bi) || (h<0.) ) return 0.;
        Float hck = h*xi - Bi;
	Float hckp1 = hck;
        Float epsilon = 1e-16;
	Float residu = 1.;
	size_t k_newton_max = 1000;
        for (size_t k_newton = 0 ; k_newton <= k_newton_max; k_newton++){
        	hckp1 = hck - F(hck, h, xi, Dam1, Bi_star, Bi)/F_prime(hck, xi, Dam1, Bi_star, Bi);
		residu = abs(hckp1 - hck);
		hck = hckp1;
		if ( (k_newton > 3) && (residu <= epsilon) ) break;
	}
	return hck;
}


 
