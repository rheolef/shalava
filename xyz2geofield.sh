#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
xe=`awk {'print $1'} tmp/domain_input.gdat`
ye=`awk {'print $2'} tmp/domain_input.gdat`
south=`awk {'print $3'} tmp/domain_input.gdat`
est=`awk {'print $4'} tmp/domain_input.gdat`
north=`awk {'print $5'} tmp/domain_input.gdat`
west=`awk {'print $6'} tmp/domain_input.gdat`
reso=`awk {'print 0.01*$7'} tmp/domain_input.gdat`
reso_dim=`awk {'print $7'} tmp/domain_input.gdat`
xmin=`awk {'print $8'} tmp/domain_input.gdat`
xmax=`awk {'print $9'} tmp/domain_input.gdat`
ymin=`awk {'print $10'} tmp/domain_input.gdat`
ymax=`awk {'print $11'} tmp/domain_input.gdat`

rm tmp/domain_input.gdat

echo "x_source = "$xe
echo "y_source = "$ye
echo "resolution = "$reso_dim
echo "xmin = "$xmin
echo "xmax = "$xmax
echo "ymin = "$ymin
echo "ymax = "$ymax

awk '{if (($1 >= '$xmin')&&($1<='$xmax')&&($2 >= '$ymin')&&($2<='$ymax')) {printf "%0.6f %0.6f %0.6f \n",0.01*($1-'$xmin'),0.01*($2-'$ymin'),0.01*$3}}' $1 > DEMadim.xyz

nxny=`wc -l DEMadim.xyz | awk '{print $1}'`
echo $nxny
x0=`head -n 1 DEMadim.xyz | awk '{print $2}'`
nx=`awk '{if ($2 != '$x0') {nl_val=NR; exit}} END{print nl_val-1;}' DEMadim.xyz`
ny=$((($nxny)/$nx))
echo $nx
echo $ny

xmin=`head -n 1 DEMadim.xyz | awk '{print $1}'`
xmax=`tail -n 1 DEMadim.xyz | awk '{print $1}'`
ymin=`tail -n 1 DEMadim.xyz | awk '{print $2}'`
ymax=`head -n 1 DEMadim.xyz | awk '{print $2}'`

$2/mkgeo_grid_2d_var -t $(($nx-1)) $(($ny-1)) -a $xmin -b $xmax -c $ymin -d $ymax -order-ji -v4 > DEM.geo
geo -upgrade DEM.geo > tmp.geo
mv tmp.geo DEM.geo

> DEM.field
echo "field" >> DEM.field
echo "1 $nxny" >> DEM.field
echo "DEM.geo" >> DEM.field
echo "P1" >> DEM.field
awk '{print $3}' DEMadim.xyz > z_tmp
cat DEM.field z_tmp > DEM_tmp
mv DEM_tmp DEM.field
rm z_tmp
rm DEMadim.xyz

> grille.dmn
echo "EdgeDomainNames" >> grille.dmn
echo "4" >> grille.dmn
echo "bottom" >> grille.dmn
echo "right" >> grille.dmn
echo "top" >> grille.dmn
echo "left" >> grille.dmn

> grille.bamgcad
echo "MeshVersionFormatted" >> grille.bamgcad
echo "    0" >> grille.bamgcad
echo "  Dimension" >> grille.bamgcad
echo "    2" >> grille.bamgcad
echo "  Vertices" >> grille.bamgcad
echo "    4" >> grille.bamgcad
echo "    $xmin $ymin 1" >> grille.bamgcad
echo "    $xmax $ymin 2" >> grille.bamgcad
echo "    $xmax $ymax 3" >> grille.bamgcad
echo "    $xmin $ymax 4" >> grille.bamgcad
echo "  Edges" >> grille.bamgcad
echo "    4" >> grille.bamgcad
echo "    1  2     101" >> grille.bamgcad
echo "    2  3     102" >> grille.bamgcad
echo "    3  4     103" >> grille.bamgcad
echo "    4  1     104" >> grille.bamgcad
echo "  hVertices" >> grille.bamgcad
echo "    $reso $reso $reso $reso" >> grille.bamgcad
bamg -g grille.bamgcad -o grille.bamg -nbv 5000000
bamg2geo grille.bamg grille.dmn > grille.geo
