\documentclass[10pt,a4paper]{article}
% packages
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{textcomp}
% \usepackage[francais]{babel}
\usepackage{graphicx}
\usepackage{float}
\usepackage[dvips,lmargin=2cm,rmargin=2cm,tmargin=1cm,bmargin=1cm]{geometry}
\usepackage{color}
\usepackage[dvipsnames]{xcolor}
\usepackage{epsfig}
\usepackage[babel=true]{csquotes}
\setlength{\parindent}{0em}
\usepackage{cellspace} 
\cellspacetoplimit=4pt 
\cellspacebottomlimit=4pt
\newcommand{\mimi}[2]{ \begin{tabular}{l} {#1} \\ {#2} \\ \end{tabular} }
\pagestyle{empty}
\begin{document}
%test1
\begin{center} \textbf{\Huge{Shalava}} \\
~\\
\textbf{\large{A GUI for simulation of effusive lava flow}} \\
~\\
Noé Bernabeu \\
~\\
Version 1.0, update May 2019
\end{center}

\section*{Introduction}
Shalava is a tool to predict the evolution of an effusive lava flow (old, on-going or fictitious). It is composed of a GUI (Graphical User Interface) developed in Python3
combined with a C++ code for the fluid dynamics simulation part.
The model is based on equations and numerical
approaches from the paper \cite{BerSarHar-2018}. The flow simulation code uses a finite element method computing with the C++ library Rheolef \cite{rheolef}. The GUI
provides a nice and quickly interactive fashion to input the set of data required to start the numerical simulation. Shalava allows to view the result of the current 3D simulation, in time
and in space, with output vtk format files readable with the software ParaView \cite{paraview}.

\section{Getting started}

Let's see in this section how to start a simulation of a lava flow on a simple example step by step.

\subsection{How to open the interface}

In a Terminal, create a folder for your new project (e.g. first$\_$project) and go inside:
\begin{verbatim}
  mkdir first_project 
  cd first_project
\end{verbatim}
Open the inteface with:
\begin{verbatim}
  shalava
\end{verbatim}
\begin{figure}[h!]
  \center \includegraphics[height=5cm]{shalava_home.png}
  \caption{Home page of Shalava}
\end{figure}

They are two chooses:  \textcolor{blue}{Start simulation} or \textcolor{blue}{Load input}. The first button is to start a new simulation from zero, the second is to load
a file which already contains all the required data values. For this first example, click on the button \textcolor{blue}{Start simulation}. Now, you will be able to input the data
for the simulation.

\subsection{Enter the data values}

The input of data is separated in four tabs (\textcolor{blue}{Domain}, \textcolor{blue}{Fluid}, \textcolor{blue}{Vents}, 
\textcolor{blue}{Forest}). The last tab \textcolor{blue}{Simulation} is the last step to start the simulation.

\subsubsection{Domain tab}

The fisrt tab \textcolor{blue}{Domain} concerns the geographical data and the computational domain parameters.\\

$\bullet$ Three Digital Elevation Model are included in Shalava, leave the selection on \textcolor{blue}{Reunion island (2008)} and do not change the
\textcolor{blue}{UTM grid zone}. \\

$\bullet$ Now, specify the UTM (Universal Transverse Mercator) coordinates: enter 368500 for the east coordinate (\textcolor{blue}{main vent position E})
and 7650600 for the north one (\textcolor{blue}{main vent position N}).\\

$\bullet$ The next step is to specify the size (in meter) of the computational domain around the main vent in the four cardinal direction. Enter 800 for \textcolor{blue}{South}, 
3000 for \textcolor{blue}{East}, 800 for \textcolor{blue}{North} and 500 for \textcolor{blue}{West}.\\

$\bullet$ Finally, specify in \textcolor{blue}{resolution} the minimal size (in meter) of mesh cells which will be used during the simulation, enter 20.

\begin{figure}[h!]
  \center \includegraphics[height=8cm]{domain_tab.png}
  \caption{The domain tab}
\end{figure}

$\bullet$ Optionally, by clicking on the button \textcolor{blue}{View map}, you can preview in your Internet browser an interactive OpenStreetMap with the localization of the vent, 
the area of the computational domain
and the full surface included and available in the selected DEM.

\begin{figure}[h!]
  \center \includegraphics[height=8cm]{osm_domain.png}
  \caption{OpenStreetMap with the vent location %\includegraphics[height=5mm]{icon_eruption_doc.png}
  the simulation domain (blue rectangle) and the DEM boundaries (black rectangle)}
\end{figure}

$\bullet$ Before to go to the next tab, click on the button \textcolor{blue}{Generate mesh and DEM files} to edit the mesh file (.geo Rheolef format) and the topography file (.field Rheolef format)
required to start the simulation. This step may takes a few minutes. You can view the barely edited topography in ParaView by clicking on the button \textcolor{blue}{View DEM with ParaView}.

\subsubsection{Fluid tab}

This tab concerns the fluid characteristics based on the Bingham behavior law.\\

$\bullet$ Enter 2200 in \textcolor{blue}{rho} (the density), 50000 in \textcolor{blue}{mu0} (the viscosity in Pa.s) and 35000 in \textcolor{blue}{tau} (the yield stress in Pa). 

\begin{figure}[h!]
  \center \includegraphics[height=8cm]{tab_fluid.png}
  \caption{The fluid characteristics tab}
\end{figure}

\subsubsection{Vents tab}

This tab concerns the vent locations (there may be several) and their flow rate setting.
The UTM coordinate \textcolor{blue}{xe} and \textcolor{blue}{ye} are locked for the main vent 
because the domain has been
created around this point. For extra vents, you can specify these coordinates in the authorized range delimited by the computation domain. If
you want to change the main vent coordinates, you have to return at the \textcolor{blue}{domain} tab, modify the values and generate again the mesh and topography files.
For each vent, you have to select the type of flow rate (\textcolor{blue}{constant} or \textcolor{blue}{variable}). For constant flow rate, you have to specify the flow rate \textcolor{blue}{Q} (m$^3.s^{-1}$), 
the radius \textcolor{blue}{R} (in meter, with suppose that all vents are circulars) and the period of activity (\textcolor{blue}{ti} the start of the activity, 
\textcolor{blue}{tf} the end of the activity, in second).
For variable flow rate, you have to load a two-columns file (time(s) and flow rate (m$^3.s^{-1}$), with time ranked in ascending order) and specify the radius.\\

$\bullet$ For this example, let's consider only one vent (the main vent). Select a constant flow rate and enter 50 in \textcolor{blue}{Q}, 
30 in \textcolor{blue}{R}, 0 in \textcolor{blue}{ti}
and 43200 in \textcolor{blue}{tf} (6 hours converted in second).

\begin{figure}[h!]
  \center \includegraphics[height=8cm]{vents_tab.png}
  \caption{The vents tab}
\end{figure}

\subsubsection{Forest tab}

The vegetation can be taken into account in Shalava, by following the model proposed in \cite{BerSarHar-2018}.
In this tab, you can specify if there is forest or not on the domain. By selecting \textcolor{blue}{Everywhere}, the vegetation covers all the computational domain.
By selecting \textcolor{blue}{Region}, you have to specify yourself the boundary of forest regions by polygonal lines. 
Specify for each vertex its UTM coordinates (E,N). For \textcolor{blue}{Everywhere} and \textcolor{blue}{Region} option, you have to specify 
the tree configuration with \textcolor{blue}{d} the diameter of the trunks, \textcolor{blue}{M} the
space between them and the \textcolor{blue}{Tree packing}.\\

$\bullet$ For this example, in \textcolor{blue}{Presence of forest}, select \textcolor{blue}{No}.

\subsection{Start the simulation}

The launch of the simulation is done in the last tab \textcolor{blue}{Simulation}. If you want to reproduce an old lava flow, you can load a shape file of the real deposit
zone (.kml or .geojson format) to compare with your simulation. Before launch the simulation, you can specify the number of cores for parallel computing.\\

$\bullet$ Finally, click on the button \textcolor{blue}{Start the simulation}. The real time and the simulated time will show up (in second).

\subsection{View the result of simulation}

At any time, you can view the progression of your simulation by clicking on the button \textcolor{blue}{View simulation}. The results are opened in the software ParaView.

\begin{figure}[h!]
  \center \includegraphics[height=8cm]{paraview.png}
  \caption{The simulation in ParaView}
\end{figure}

\bibliographystyle{plain}
\bibliography{biblio}

\end{document}
