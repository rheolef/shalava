///
/// This file is part of Shalava.
///
/// Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
///
/// Shalava is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Shalava is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Shalava; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
class shallow_lava_HB {
public:
  typedef field value_type;
  typedef Float float_type;
  shallow_lava_HB (Float Bi, field Dam1_h, field Bi_star, field h, field h_old, field fh, field ws, Float coeff1, Float coeff2, Float coeff3, const space& Xh1, const space& Xh21);
  field residue          (const field& phi) const;
  void update_derivative (const field& phi) const;
  field derivative_solve      (const field& mrh) const;
  field derivative_trans_mult (const field& mrh) const;
  Float space_norm       (const field& phi) const;
  Float dual_space_norm  (const field& mrh) const;
  Float duality_product  (const field& mrh, const field& msh) const;
  Float Bi;
  field Dam1_h;
  field Bi_star_h;
  field h;
  field h_old;
  field fh;
  field ws;
  field tau_h;
  field mu0_h;
  Float coeff1;
  Float coeff2;
  Float coeff3;
  space Xh;
  space Xh2;
  field lh;
  form m;
  solver sm;
  quadrature_option_type qopt;
  quadrature_option_type qopt2;
  mutable form a1;
  mutable solver sa1;
};
#include "lavaview1.icc"
#include "lavaview2.icc"
