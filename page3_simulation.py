#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
PKGDATADIR="."
from tkinter import *
from tkinter import ttk

import importlib.machinery
vertical_canvas = importlib.machinery.SourceFileLoader('vertical_canvas',PKGDATADIR+'/vertical_canvas.py').load_module()
lavaview_variable = importlib.machinery.SourceFileLoader('lavaview_variable',PKGDATADIR+'/lavaview_variable.py').load_module()
hot_spot_Frame = importlib.machinery.SourceFileLoader('hot_spot_Frame',PKGDATADIR+'/hot_spot_Frame.py').load_module()
from vertical_canvas import *
from lavaview_variable import *
from hot_spot_Frame import *

class page3():
	"""
	A class to create the first page about the domain
	"""
	def __init__(self, parent, mytitle, *args, **kw):
		self.parent=parent
		f = parent.n.add(mytitle)
		parent.n.tab(mytitle).configure(background=dark_grey,fg="white",state="disabled")
		self.scrollable_canvas  = VerticalScrolledFrame(f)
		self.scrollable_canvas.pack(fill = 'both', expand = 1)
		parent.n.tab(mytitle).bind('<1>',self.reset_the_view,add="+") #reset the view after click on tabs
		main_window = self.scrollable_canvas.interior
		self.hot_spot_Frame = Lavaview_hot_spot_Frame(main_window,"vent")
		self.hot_spot_Frame.vent_frame_list[0].xe_variable.MyEntry.configure(state="disabled")
		self.hot_spot_Frame.vent_frame_list[0].ye_variable.MyEntry.configure(state="disabled")
		self.hot_spot_Frame.vent_frame_list[0].xe_variable.Myscale.configure(state="disabled")
		self.hot_spot_Frame.vent_frame_list[0].ye_variable.Myscale.configure(state="disabled")
		self.hot_spot_Frame.pack(padx = 5, pady = 5)
		Label(self.hot_spot_Frame,text="Hot spot characteristics",font=("Helvetica", 16, "bold"), bg=dark_grey,fg= "white").grid(row=0,column=0,padx = 5, pady = 5)
		Frame_for_button=Frame(main_window,bg=dark_grey)
		Frame_for_button.pack(padx = 5, pady = 5)
		previous_from_3_to_2 = ttk.Button(Frame_for_button, text = 'Previous', command = lambda:parent.n.previouspage())
		next_from_3_to_4 = ttk.Button(Frame_for_button, text = 'Next', command = lambda:parent.n.nextpage())
		previous_from_3_to_2.grid(row=0,column=0, padx = 5, pady = 25)
		next_from_3_to_4.grid(row=0,column=1, padx = 5, pady = 25)
	def reset_the_view(self, event):
		self.scrollable_canvas.canvas.xview_moveto(0)
		self.scrollable_canvas.canvas.yview_moveto(0)
		"""xe = float(self.parent.p1.main_vent_xe_variable.Myvar.get())
		ye = float(self.parent.p1.main_vent_ye_variable.Myvar.get())
		south = float(self.parent.p1.south_variable.Myvar.get())
		est = float(self.parent.p1.est_variable.Myvar.get())
		north = float(self.parent.p1.north_variable.Myvar.get())
		west = float(self.parent.p1.west_variable.Myvar.get())
		xmin_value = xe-west
		xmax_value = xe+est
		ymin_value = ye-south
		ymax_value = ye+north
		self.parent.p1.update_scale(self.hot_spot_Frame.vent_frame_list[0].xe_variable,xmin_value,xmax_value)
		self.parent.p1.update_scale(self.hot_spot_Frame.vent_frame_list[0].ye_variable,ymin_value,ymax_value)"""
