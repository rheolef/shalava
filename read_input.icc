///
/// This file is part of Shalava.
///
/// Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
///
/// Shalava is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Shalava is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Shalava; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
din >> catchmark("south") >> south;
din >> catchmark("est") >> est;
din >> catchmark("north") >> north;
din >> catchmark("west") >> west;
din >> catchmark("reso") >> reso;
din >> catchmark("rho") >> rho;
din >> catchmark("mu0") >> mu0;
din >> catchmark("tau") >> tau;
din >> catchmark("nb_vents") >> nb_vents;

for (size_t i=1; i<=nb_vents; i++){
	din >> catchmark("flow_rate_filename_"+itos(i)) >> flow_rate_filenames[i];
	din >> catchmark("Q_"+itos(i)) >> Q[i];
	din >> catchmark("R_"+itos(i)) >> R[i];
	Float vent_x;
	Float vent_y;
	din >> catchmark("xe_"+itos(i)) >> vent_x;
	din >> catchmark("ye_"+itos(i)) >> vent_y;
	xe[i] = point(vent_x,vent_y);
	din >> catchmark("ti_"+itos(i)) >> ti[i];
	din >> catchmark("tf_"+itos(i)) >> tf[i];
}

din >> catchmark("presence_of_trees") >> presence_of_trees;

if (presence_of_trees !="no") {
	din >> catchmark("d") >> d;
	din >> catchmark("M") >> M;
	din >> catchmark("arrangement_of_trees") >> arrangement_of_trees;
	if (presence_of_trees =="region"){
		din >> catchmark("nb_regions") >> nb_regions;
		for (size_t i=1; i<=nb_regions; i++){
			din >> catchmark("nb_vertex_"+itos(i)) >> nb_vertex[i];
			for (size_t j=1; j<=nb_vertex[i]; j++){
				Float vertex_x;
				Float vertex_y;
				din >> catchmark("vertex_x_"+itos(i)+"_"+itos(j)) >> vertex_x;
				din >> catchmark("vertex_y_"+itos(i)+"_"+itos(j)) >> vertex_y;
				vertex[i][j] = point(vertex_x,vertex_y);
			}
		}
	}
}
din >> catchmark("final_shape") >> final_shape;
