///
/// This file is part of Shalava.
///
/// Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
///
/// Shalava is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Shalava is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Shalava; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
Float shallow_lava_HB::dual_space_norm (const field& mrh) const {
  field rh (Xh, 0);
  rh.set_u() = sm.solve (mrh.u());
  return rh.max_abs();
}
Float shallow_lava_HB::space_norm (const field& phi) const {
  return sqrt (m(phi,phi));
}
Float shallow_lava_HB::duality_product (const field& mrh, const field& msh) const {
  field rh (Xh, 0);
  rh.set_u() = sm.solve (mrh.u());
  return dual (rh, msh);
}
field shallow_lava_HB::derivative_trans_mult (const field& mrh) const {
  field rh (Xh, 0);
  rh.set_u() = sm.solve(mrh.u());
  field mgh = a1*rh;
  mgh.set_b() = 0;
  return mgh;
}
