#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
from tkinter import *
import Pmw

dark_grey = "#282827"
light_grey = "#484845"

class Lavaview_variable():
	def __init__(self, parent, name="", ballon_help="", initial_value=0, row_value=0, column_value=0, bg_color=dark_grey):
		self.name=name
		self.MyLabel = Label(parent, text = name+' :', bg=bg_color, fg = "white")
		self.initial_value=initial_value
		self.row_value=row_value
		self.column_value=column_value
		self.bg_color=bg_color
		self.Myvar = StringVar()
		self.Myvar.set(str(initial_value))
		self.MyEntry = Entry(parent, textvariable=self.Myvar, validate="focusout", validatecommand=self.callback, width=10, font=("Helvetica", 16))
		self.MyLabel.grid(sticky="E", row = row_value, column = column_value, padx = 5, pady = 5)
		self.MyEntry.grid(sticky='ew', row = row_value, column = column_value+1, padx = 5, pady = 5)
		self.balloon = Pmw.Balloon(parent)
		self.balloon.bind(self.MyLabel, ballon_help)
	def callback(self):
		return True

class Lavaview_scale_variable(Lavaview_variable):
	def __init__(self, parent, min_scale_value, max_scale_value, resolution, scale_span, *args, **kw): #, girl, min_girl, max_girl, 
		Lavaview_variable.__init__(self, parent, *args, **kw)
		self.Myscalevar = DoubleVar()
		self.Myscalevar.set(self.initial_value)
		self.min_scale_value = DoubleVar()
		self.max_scale_value = DoubleVar()
		self.min_scale_value.set(min_scale_value)
		self.max_scale_value.set(max_scale_value)
		self.my_interval = (self.max_scale_value.get() - self.min_scale_value.get())/1.0
		self.Myscale = Scale(parent, from_=self.min_scale_value.get(), to=self.max_scale_value.get(),tickinterval=0, variable=self.Myscalevar, orient=HORIZONTAL, bg=self.bg_color, fg="white", highlightthickness=0, length=200, resolution=-1, digits = 5, showvalue=0, troughcolor=light_grey)
		self.Myscale.grid(row = self.row_value, column = self.column_value+2, columnspan = scale_span, padx = 5, pady = 5)

class Lavaview_vertical_scale_variable():
	def __init__(self, parent, name="", initial_value=0, row_value=0, column_value=0, bg_color=dark_grey, min_scale_value=0, max_scale_value=0, resolution=1):
		self.name=name
		self.initial_value=initial_value
		self.row_value=row_value
		self.column_value=column_value
		self.Myvar = StringVar()
		self.Myvar.set(str(initial_value))
		self.MyEntry = Entry(parent, textvariable=self.Myvar, validate="focusout", validatecommand=self.callback, width=10, font=("Helvetica", 16))
		self.MyEntry.grid(sticky='ew', row = row_value+1, column = column_value, padx = 5, pady = (0,20))
		self.Myscalevar = DoubleVar()
		self.Myscalevar.set(self.initial_value)
		self.min_scale_value = DoubleVar()
		self.max_scale_value = DoubleVar()
		self.min_scale_value.set(min_scale_value)
		self.max_scale_value.set(max_scale_value)
		self.my_interval = (self.max_scale_value.get() - self.min_scale_value.get())/1.0
		self.Myscale = Scale(parent, from_=self.min_scale_value.get(), to=self.max_scale_value.get(),tickinterval=0, variable=self.Myscalevar, orient=HORIZONTAL, bg=bg_color, fg="white", highlightthickness=0, length=200, resolution=-1, digits = 5, showvalue=0, troughcolor=light_grey)
		self.Myscale.grid(row = self.row_value, column = self.column_value, padx = 5, pady = 5)
	def callback(self):
		return True