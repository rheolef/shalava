///
/// This file is part of Shalava.
///
/// Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
///
/// Shalava is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Shalava is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Shalava; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
/* --------- Simulation domain --------- */
Float south = 1000; // size of the domain simulation in the south of the 1st vent (m)
Float est   = 1000; // size of the domain simulation in the est of the 1st vent (m)
Float north = 1000;  // size of the domain simulation in the north of the 1st vent (m)
Float west  = 1000; // size of the domain simulation in the west of the 1st vent (m)
Float reso  = 10;    // resolution of simulation (m) (= or > that your DEM resolution)

/* --------- Fluid caracteristics (Bingham model) ---------*/
Float rho = 2200;  // fluid density
Float mu0 = 1e2;   // viscosity (Pa.s)
Float tau = 100.;  // yield stress (Pa) (tau=0 for Newtonian fluids)


/* --------- Hot spot caracteristics (circular vents) ---------*/

size_t nb_vents = 5; // number of vents (between 1 and 5, for nb>5, add a new vent caracteristics bloc)
vector<string> flow_rate_filenames(100,"none");
vector<Float> Q(100,0.);
vector<Float> R(100,0.);
vector<point> xe(100,point(0.,0.));
vector<Float> ti(100,0.);
vector<Float> tf(100,0.);

// Caracteristics of vent 1
Q[1]  = 150./5.;               // rate of flow (m3.s-1) for the vent 1
R[1]  = 20.;                   // radius of the vent 1 (m)
xe[1] = point(7.77869,27.782); // UTM coordinate of the center of the vent 1
ti[1] = 0.;                    // time (s) for the begining of the activity of the vent 1 (leave zero for the fist vent)
tf[1] = 18000.;                // time (s) for the stoppage of the activity of the vent 1

// If (nb_vents > 1) please complete the Caracteristics of vent 2
Q[2]  = 150./5.;                // rate of flow (m3.s-1) for the vent 2
R[2]  = 20.;                    // radius of the vent 2 (m)
xe[2] = point(8.50924,27.9966); // UTM coordinate of the center of the vent 2
ti[2] = 0.;                     // time (s) for the begining of the activity of the vent 2
tf[2] = 18000.;                 // time (s) for the stoppage of the activity of the vent 2

// If (nb_vents > 2) please complete the Caracteristics of vent 3
Q[3]  = 150./5.;                // rate of flow (m3.s-1) for the vent 3
R[3]  = 20.;                    // radius of the vent 3 (m)
xe[3] = point(9.23979,28.2111); // UTM coordinate of the center of the vent 3
ti[3] = 0.;                     // time (s) for the begining of the activity of the vent 3
tf[3] = 18000.;                 // time (s) for the stoppage of the activity of the vent 3

// If (nb_vents > 3) please complete the Caracteristics of vent 4
Q[4]  = 150./5.;                // rate of flow (m3.s-1) for the vent 4
R[4]  = 20.;                    // radius of the vent 4 (m)
xe[4] = point(9.8844,28.3828);  // UTM coordinate of the center of the vent 4
ti[4] = 0.;                     // time (s) for the begining of the activity of the vent 4
tf[4] = 18000.;                 // time (s) for the stoppage of the activity of the vent 4

// If (nb_vents > 4) please complete the Caracteristics of vent 5
Q[5]  = 150./5.;                // rate of flow (m3.s-1) for the vent 5
R[5]  = 20.;                    // radius of the vent 5 (m)
xe[5] = point(10.5032,28.4772); // UTM coordinate of the center of the vent 5
ti[5] = 0.;                     // time (s) for the begining of the activity of the vent 5
tf[5] = 18000.;                 // time (s) for the stoppage of the activity of the vent 5

// If (nb_vents > 5) please add and complete a new vent caracteristics bloc and increment the indice between hook: [i+1]


/* ---------Forest caracteristics ---------*/

string presence_of_trees = "no"; // Answer "everywhere", "region" or "no"

// If (presence_of_trees = "everywhere" or "region") please complete the following part
Float d = 0.4;                           // diametre of trees (m)
Float M = 10.4;                          // distance between two centers of tree
string arrangement_of_trees = "square";  // Answer "square" or "hexagonal"

// If (presence_of_trees = "region") please complete the following part
size_t nb_regions = 0; // number of separate region (described as polygon)
vector<size_t> nb_vertex(100,0);
vector<vector<point> > vertex(100,vector<point>(100));

// Localisation of region 1
/*nb_vertex[1] = 4;               // number of vertex to delimit the region10
vertex[1][1] = point(5.,5.);    // UTM coordinate of vertex(1,1)
vertex[1][2] = point(10.,5.);   // UTM coordinate of vertex(1,2)
vertex[1][3] = point(10.,10.);  // UTM coordinate of vertex(1,3)
vertex[1][4] = point(5.,10.);   // UTM coordinate of vertex(1,4)
// If (nb_vertex[0] > 3) please add extra vertex

// Localisation of region 2
nb_vertex[2] = 4;               // number of vertex to delimit the region 2
vertex[2][1] = point(15.,15.);  // UTM coordinate of vertex(2,1)
vertex[2][2] = point(20.,15.);  // UTM coordinate of vertex(2,2)
vertex[2][3] = point(20.,20.);  // UTM coordinate of vertex(2,3)
vertex[2][4] = point(15.,20.);  // UTM coordinate of vertex(2,4)
// If (nb_vertex[1] > 4) please add extra vertex

// If (nb_regions > 2) please add and complete a "Localisation of region" bloc and increment the first indice between hook: [i+1,.]*/


/* ---------Simulation part ---------*/
string final_shape = "none";
size_t nb_shape_vertex = 0.;
vector<point> shape_vertex(1e4,point(0.,0.));


