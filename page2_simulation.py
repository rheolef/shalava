#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
from tkinter import *
from tkinter import ttk
from vertical_canvas import *
from lavaview_variable import *

class page2():
	"""
	A class to create the first page about the domain
	"""
	def __init__(self, parent, mytitle, *args, **kw):
		f = parent.n.add(mytitle)
		parent.n.tab(mytitle).configure(background=dark_grey,fg="white",state="disabled")
		self.scrollable_canvas  = VerticalScrolledFrame(f)
		self.scrollable_canvas.pack(fill = 'both', expand = 1)
		parent.n.tab(mytitle).bind('<1>',self.reset_the_view,add="+") #reset the view after click on tabs
		main_window = self.scrollable_canvas.interior
		# Create a frame to center all widgets
		center_Frame = Frame(main_window)
		center_Frame.pack()
		center_Frame.configure(borderwidth=0, bg=dark_grey)
		Label(center_Frame,text="Fluid characteristics \n (Bingham model)",font=("Helvetica", 16, "bold"), bg=dark_grey, fg= "white").grid(row=0,column=0,columnspan=2,padx = 5, pady = 5)
		self.rho_variable = Lavaview_variable(center_Frame,"rho", "density", 2200,1,0, dark_grey)
		self.mu_variable = Lavaview_variable(center_Frame,"mu0", "viscosity", 4000,2,0,dark_grey)
		self.tau_variable = Lavaview_variable(center_Frame,"tau", "yield stress", 10,3,0,dark_grey)
		Frame_for_button=Frame(main_window,bg=dark_grey)
		Frame_for_button.pack(padx = 5, pady = 5)
		previous_from_2_to_1 = ttk.Button(Frame_for_button, text = 'Previous', command = lambda:parent.n.previouspage())
		next_from_2_to_3 = ttk.Button(Frame_for_button, text = 'Next', command = lambda:parent.n.nextpage())
		previous_from_2_to_1.grid(row=0,column=0, padx = 5, pady = 25)
		next_from_2_to_3.grid(row=0,column=1, padx = 5, pady = 25)
	def reset_the_view(self, event):
		self.scrollable_canvas.canvas.xview_moveto(0)
		self.scrollable_canvas.canvas.yview_moveto(0)
