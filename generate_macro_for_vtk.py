#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
import os

class write_vtk_macro():
	"""
	A class to create vtk macro
	"""
	def __init__(self, filename="none",region_var=1, *args, **kw):
		self.filename = filename
		self.region_var=region_var
		self.pwd = os.popen("pwd").read().rstrip()

	def view_in_vtk(self):
		self.write_fh_h_vtk()
		if (self.filename!="none"):
			self.write_shape_vtk()
		if (self.region_var==3):
			self.write_forest_vtk()

		my_file = open("tmp/macro_for_vtk.py", "a")

		#### saving camera placements for all active views

		# current camera placement for renderView1
		my_file.write("renderView1.InteractionMode = \'2D\'\n")
		my_file.write("renderView1.CameraPosition = [5.004600148182362, 5.009799864143133, 10000.0]\n")
		my_file.write("renderView1.CameraFocalPoint = [5.004600148182362, 5.009799864143133, 0.0]\n")
		my_file.write("renderView1.CameraParallelScale = 7.056925684789655\n")

		#### uncomment the following to render all views
		# RenderAllViews()
		# alternatively, if you want to write images, you can use SaveScreenshot(...).

		my_file.close()
		os.system("paraview --script=tmp/macro_for_vtk.py &")
	def write_fh_h_vtk(self):
		my_file = open("tmp/macro_for_vtk.py", "w")
		nb_vtk = os.popen("ls lava-*.vtk -Al | wc -l").read()
		list_vtk=[]
		for i in range(int(nb_vtk)):
			list_vtk.append("\""+self.pwd+"/lava-"+str(i)+".vtk\"")
		my_file = open("tmp/macro_for_vtk.py", "w") #erase 
		#### import the simple module from the paraview
		my_file.write("from paraview.simple import *\n")
		#### disable automatic camera reset on 'Show'
		my_file.write("paraview.simple._DisableFirstRenderCameraReset()\n")

		# create a new 'Legacy VTK Reader'
		my_file.write("fh_smoothvtk = LegacyVTKReader(FileNames=[\'"+self.pwd+"/fh_smooth.vtk\'])\n")

		# get active view
		my_file.write("renderView1 = GetActiveViewOrCreate(\'RenderView\')\n")
		# uncomment following to set a specific view size
		# renderView1.ViewSize = [729, 517]

		# get color transfer function/color map for 'scalar'
		my_file.write("scalarLUT = GetColorTransferFunction(\'scalar\')\n")
		my_file.write("scalarLUT.RGBPoints = [10.779187202453613, 0.231373, 0.298039, 0.752941, 11.1914381980896, 0.865003, 0.865003, 0.865003, 11.603689193725586, 0.705882, 0.0156863, 0.14902]\n")
		my_file.write("scalarLUT.ScalarRangeInitialized = 1.0\n")

		# show data in view
		my_file.write("fh_smoothvtkDisplay = Show(fh_smoothvtk, renderView1)\n")
		# trace defaults for the display properties.
		my_file.write("fh_smoothvtkDisplay.ColorArrayName = [\'POINTS\', \'scalar\']\n")
		my_file.write("fh_smoothvtkDisplay.LookupTable = scalarLUT\n")
		my_file.write("fh_smoothvtkDisplay.OSPRayScaleArray = \'scalar\'\n")
		my_file.write("fh_smoothvtkDisplay.OSPRayScaleFunction = \'PiecewiseFunction\'\n")
		my_file.write("fh_smoothvtkDisplay.GlyphType = \'Arrow\'\n")
		my_file.write("fh_smoothvtkDisplay.ScalarOpacityUnitDistance = 0.4929976581906386\n")

		# reset view to fit data
		my_file.write("renderView1.ResetCamera()\n")

		#changing interaction mode based on data extents
		my_file.write("renderView1.InteractionMode = \'2D\'\n")
		my_file.write("renderView1.CameraPosition = [5.004600148182362, 5.009799864143133, 10000.0]\n")
		my_file.write("renderView1.CameraFocalPoint = [5.004600148182362, 5.009799864143133, 0.0]\n")

		# show color bar/color legend
		my_file.write("fh_smoothvtkDisplay.SetScalarBarVisibility(renderView1, True)\n")

		# get opacity transfer function/opacity map for 'scalar'
		my_file.write("scalarPWF = GetOpacityTransferFunction(\'scalar\')\n")
		my_file.write("scalarPWF.Points = [10.779187202453613, 0.0, 0.5, 0.0, 11.603689193725586, 1.0, 0.5, 0.0]\n")
		my_file.write("scalarPWF.ScalarRangeInitialized = 1\n")

		# create a new 'Warp By Scalar'
		my_file.write("warpByScalar1 = WarpByScalar(Input=fh_smoothvtk)\n")
		my_file.write("warpByScalar1.Scalars = [\'POINTS\', \'scalar\']\n")

		# Properties modified on warpByScalar1
		my_file.write("warpByScalar1.ScaleFactor = 2.0\n")

		# show data in view
		my_file.write("warpByScalar1Display = Show(warpByScalar1, renderView1)\n")
		# trace defaults for the display properties.
		my_file.write("warpByScalar1Display.ColorArrayName = [\'POINTS\', \'scalar\']\n")
		my_file.write("warpByScalar1Display.LookupTable = scalarLUT\n")
		my_file.write("warpByScalar1Display.OSPRayScaleArray = \'scalar\'\n")
		my_file.write("warpByScalar1Display.OSPRayScaleFunction = \'PiecewiseFunction\'\n")
		my_file.write("warpByScalar1Display.GlyphType = \'Arrow\'\n")
		my_file.write("warpByScalar1Display.ScalarOpacityUnitDistance = 0.4963511142733224\n")

		# hide data in view
		my_file.write("Hide(fh_smoothvtk, renderView1)\n")

		# show color bar/color legend
		my_file.write("warpByScalar1Display.SetScalarBarVisibility(renderView1, True)\n")

		# hide color bar/color legend
		my_file.write("warpByScalar1Display.SetScalarBarVisibility(renderView1, False)\n")

		# create a new 'Legacy VTK Reader'
		my_file.write("lava = LegacyVTKReader(FileNames=["+",".join(list_vtk)+"])\n")

		# get animation scene
		my_file.write("animationScene1 = GetAnimationScene()\n")

		# update animation scene based on data timesteps
		my_file.write("animationScene1.UpdateAnimationUsingDataTimeSteps()\n")

		# get color transfer function/color map for 'time'
		my_file.write("timeLUT = GetColorTransferFunction(\'time\')\n")
		my_file.write("timeLUT.RGBPoints = [0.1502791792154312, 0.231373, 0.298039, 0.752941, 0.15027993061884126, 0.865003, 0.865003, 0.865003, 0.15028068202225128, 0.705882, 0.0156863, 0.14902]\n")
		my_file.write("timeLUT.ScalarRangeInitialized = 1.0\n")

		# show data in view
		my_file.write("lavaDisplay = Show(lava, renderView1)\n")
		# trace defaults for the display properties.
		my_file.write("lavaDisplay.ColorArrayName = [\'POINTS\', \'time\']\n")
		my_file.write("lavaDisplay.LookupTable = timeLUT\n")
		my_file.write("lavaDisplay.OSPRayScaleArray = \'time\'\n")
		my_file.write("lavaDisplay.OSPRayScaleFunction = \'PiecewiseFunction\'\n")
		my_file.write("lavaDisplay.GlyphType = \'Arrow\'\n")
		my_file.write("lavaDisplay.ScalarOpacityUnitDistance = 1.601868418129527\n")

		# show color bar/color legend
		my_file.write("lavaDisplay.SetScalarBarVisibility(renderView1, True)\n")

		# get opacity transfer function/opacity map for 'time'
		my_file.write("timePWF = GetOpacityTransferFunction(\'time\')\n")
		my_file.write("timePWF.Points = [0.1502791792154312, 0.0, 0.5, 0.0, 0.15028068202225128, 1.0, 0.5, 0.0]\n")
		my_file.write("timePWF.ScalarRangeInitialized = 1\n")

		# create a new 'Warp By Scalar'
		my_file.write("warpByScalar2 = WarpByScalar(Input=lava)\n")
		my_file.write("warpByScalar2.Scalars = [\'POINTS\', \'time\']\n")

		# Properties modified on warpByScalar2
		my_file.write("warpByScalar2.Scalars = [\'POINTS\', \'f+h\']\n")
		my_file.write("warpByScalar2.ScaleFactor = 2.01\n")

		# show data in view
		my_file.write("warpByScalar2Display = Show(warpByScalar2, renderView1)\n")
		# trace defaults for the display properties.
		my_file.write("warpByScalar2Display.ColorArrayName = [\'POINTS\', \'time\']\n")
		my_file.write("warpByScalar2Display.LookupTable = timeLUT\n")
		my_file.write("warpByScalar2Display.OSPRayScaleArray = \'time\'\n")
		my_file.write("warpByScalar2Display.OSPRayScaleFunction = \'PiecewiseFunction\'\n")
		my_file.write("warpByScalar2Display.GlyphType = \'Arrow\'\n")
		my_file.write("warpByScalar2Display.ScalarOpacityUnitDistance = 1.6128639586893658\n")

		# hide data in view
		my_file.write("Hide(lava, renderView1)\n")

		# show color bar/color legend
		my_file.write("warpByScalar2Display.SetScalarBarVisibility(renderView1, True)\n")

		# set scalar coloring
		my_file.write("ColorBy(warpByScalar2Display, (\'POINTS\', \'h\'))\n")

		# rescale color and/or opacity maps used to include current data range
		my_file.write("warpByScalar2Display.RescaleTransferFunctionToDataRange(True)\n")

		# show color bar/color legend
		my_file.write("warpByScalar2Display.SetScalarBarVisibility(renderView1, True)\n")

		# get color transfer function/color map for 'h'
		my_file.write("hLUT = GetColorTransferFunction(\'h\')\n")
		my_file.write("hLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.008083454333245754, 0.865003, 0.865003, 0.865003, 0.01616690866649151, 0.705882, 0.0156863, 0.14902]\n")
		my_file.write("hLUT.ScalarRangeInitialized = 1.0\n")

		# get opacity transfer function/opacity map for 'h'
		my_file.write("hPWF = GetOpacityTransferFunction(\'h\')\n")
		my_file.write("hPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.01616690866649151, 1.0, 0.5, 0.0]\n")
		my_file.write("hPWF.ScalarRangeInitialized = 1\n")

		# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
		my_file.write("hLUT.ApplyPreset(\'Black-Body Radiation\', True)\n")

		# Properties modified on hPWF
		my_file.write("hPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.0005148696945980191, 0.9671052694320679, 0.5, 0.0, 0.01616690866649151, 1.0, 0.5, 0.0]\n")

		# Properties modified on hPWF
		my_file.write("hPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.00015446091128978878, 0.9868420958518982, 0.5, 0.0, 0.01616690866649151, 1.0, 0.5, 0.0]\n")

		# Properties modified on hPWF
		my_file.write("hPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.0, 1.0, 0.5, 0.0, 0.01616690866649151, 1.0, 0.5, 0.0]\n")

		# Properties modified on hLUT
		my_file.write("hLUT.EnableOpacityMapping = 1\n")

		# get color legend/bar for hLUT in view renderView1
		my_file.write("hLUTColorBar = GetScalarBar(hLUT, renderView1)\n")
		my_file.write("hLUTColorBar.Title = \'h\'\n")
		my_file.write("hLUTColorBar.ComponentTitle = \'\'\n")

		# Properties modified on hLUTColorBar
		my_file.write("hLUTColorBar.TitleColor = [0.0, 0.0, 0.0]\n")
		my_file.write("hLUTColorBar.TitleBold = 1\n")
		my_file.write("hLUTColorBar.TitleFontSize = 12\n")
		my_file.write("hLUTColorBar.LabelColor = [0.0, 0.0, 0.0]\n")
		my_file.write("hLUTColorBar.LabelBold = 1\n")
		my_file.write("hLUTColorBar.LabelFontSize = 12\n")
		my_file.close()
	def write_shape_vtk(self):
		my_file = open("tmp/macro_for_vtk.py", "a")
		# create a new 'Legacy VTK Reader'
		my_file.write("id_shape0vtk = LegacyVTKReader(FileNames=[\'"+self.pwd+"/id_shape-0.vtk\'])\n")

		# update animation scene based on data timesteps
		my_file.write("animationScene1.UpdateAnimationUsingDataTimeSteps()\n")

		# get color transfer function/color map for 'shape'
		my_file.write("shapeLUT = GetColorTransferFunction(\'shape\')\n")
		my_file.write("shapeLUT.ScalarRangeInitialized = 1.0\n")

		# show data in view
		my_file.write("id_shape0vtkDisplay = Show(id_shape0vtk, renderView1)\n")
		# trace defaults for the display properties.
		my_file.write("id_shape0vtkDisplay.ColorArrayName = [\'POINTS\', \'shape\']\n")
		my_file.write("id_shape0vtkDisplay.LookupTable = shapeLUT\n")
		my_file.write("id_shape0vtkDisplay.OSPRayScaleArray = \'shape\'\n")
		my_file.write("id_shape0vtkDisplay.OSPRayScaleFunction = \'PiecewiseFunction\'\n")
		my_file.write("id_shape0vtkDisplay.GlyphType = \'Arrow\'\n")
		my_file.write("id_shape0vtkDisplay.ScalarOpacityUnitDistance = 1.325402279736169\n")

		# show color bar/color legend
		my_file.write("id_shape0vtkDisplay.SetScalarBarVisibility(renderView1, True)\n")

		# get opacity transfer function/opacity map for 'shape'
		my_file.write("shapePWF = GetOpacityTransferFunction(\'shape\')\n")
		my_file.write("shapePWF.ScalarRangeInitialized = 1\n")

		# create a new 'Warp By Scalar'
		my_file.write("warpByScalar3 = WarpByScalar(Input=id_shape0vtk)\n")
		my_file.write("warpByScalar3.Scalars = [\'POINTS\', \'shape\']\n")

		# Properties modified on warpByScalar3
		my_file.write("warpByScalar3.Scalars = [\'POINTS\', \'f\']\n")
		my_file.write("warpByScalar3.ScaleFactor = 2.01\n")

		# show data in view
		my_file.write("warpByScalar3Display = Show(warpByScalar3, renderView1)\n")
		# trace defaults for the display properties.
		my_file.write("warpByScalar3Display.ColorArrayName = [\'POINTS\', \'shape\']\n")
		my_file.write("warpByScalar3Display.LookupTable = shapeLUT\n")
		my_file.write("warpByScalar3Display.OSPRayScaleArray = \'shape\'\n")
		my_file.write("warpByScalar3Display.OSPRayScaleFunction = \'PiecewiseFunction\'\n")
		my_file.write("warpByScalar3Display.GlyphType = \'Arrow\'\n")
		my_file.write("warpByScalar3Display.ScalarOpacityUnitDistance = 1.5449347398369073\n")

		# hide data in view
		my_file.write("Hide(id_shape0vtk, renderView1)\n")

		# show color bar/color legend
		my_file.write("warpByScalar3Display.SetScalarBarVisibility(renderView1, True)\n")

		# create a new 'Contour'
		my_file.write("contour1 = Contour(Input=warpByScalar3)\n")
		my_file.write("contour1.ContourBy = [\'POINTS\', \'shape\']\n")
		my_file.write("contour1.Isosurfaces = [0.5]\n")
		my_file.write("contour1.PointMergeMethod = \'Uniform Binning\'\n")

		# show data in view
		my_file.write("contour1Display = Show(contour1, renderView1)\n")
		# trace defaults for the display properties.
		my_file.write("contour1Display.ColorArrayName = [None, \'\']\n")
		my_file.write("contour1Display.OSPRayScaleArray = \'f\'\n")
		my_file.write("contour1Display.OSPRayScaleFunction = \'PiecewiseFunction\'\n")
		my_file.write("contour1Display.GlyphType = \'Arrow\'\n")

		# hide data in view
		my_file.write("Hide(warpByScalar3, renderView1)\n")
		my_file.close()
	def write_forest_vtk(self):
		my_file = open("tmp/macro_for_vtk.py", "a")
		# create a new 'Legacy VTK Reader'
		my_file.write("id_forest0vtk = LegacyVTKReader(FileNames=[\'"+self.pwd+"/id_forest-0.vtk\'])\n")

		# find source
		my_file.write("legacyVTKReader1 = FindSource('LegacyVTKReader1')\n")

		# find source
		my_file.write("warpByScalar1 = FindSource('WarpByScalar1')\n")

		# find source
		my_file.write("legacyVTKReader2 = FindSource('LegacyVTKReader2')\n")

		# find source
		my_file.write("warpByScalar2 = FindSource('WarpByScalar2')\n")

		# find source
		my_file.write("legacyVTKReader3 = FindSource('LegacyVTKReader3')\n")

		# find source
		my_file.write("warpByScalar3 = FindSource('WarpByScalar3')\n")

		# find source
		my_file.write("contour1 = FindSource('Contour1')\n")

		# get active view
		my_file.write("renderView1 = GetActiveViewOrCreate('RenderView')\n")
		# uncomment following to set a specific view size
		# renderView1.ViewSize = [1004, 339]

		# get color transfer function/color map for 'trees'
		my_file.write("treesLUT = GetColorTransferFunction('trees')\n")

		# show data in view
		my_file.write("id_forest0vtkDisplay = Show(id_forest0vtk, renderView1)\n")
		# trace defaults for the display properties.
		my_file.write("id_forest0vtkDisplay.ColorArrayName = ['POINTS', 'trees']\n")
		my_file.write("id_forest0vtkDisplay.LookupTable = treesLUT\n")
		my_file.write("id_forest0vtkDisplay.OSPRayScaleArray = 'trees'\n")
		my_file.write("id_forest0vtkDisplay.OSPRayScaleFunction = 'PiecewiseFunction'\n")
		my_file.write("id_forest0vtkDisplay.GlyphType = 'Arrow'\n")
		my_file.write("id_forest0vtkDisplay.ScalarOpacityUnitDistance = 1.273849372953254\n")

		# show color bar/color legend
		my_file.write("id_forest0vtkDisplay.SetScalarBarVisibility(renderView1, True)\n")

		# get opacity transfer function/opacity map for 'trees'
		my_file.write("treesPWF = GetOpacityTransferFunction('trees')\n")

		# create a new 'Warp By Scalar'
		my_file.write("warpByScalar4 = WarpByScalar(Input=id_forest0vtk)\n")
		my_file.write("warpByScalar4.Scalars = ['POINTS', 'trees']\n")

		# Properties modified on warpByScalar4
		my_file.write("warpByScalar4.Scalars = ['POINTS', 'f']\n")
		my_file.write("warpByScalar4.ScaleFactor = 2.01\n")

		# show data in view
		my_file.write("warpByScalar4Display = Show(warpByScalar4, renderView1)\n")
		# trace defaults for the display properties.
		my_file.write("warpByScalar4Display.ColorArrayName = ['POINTS', 'trees']\n")
		my_file.write("warpByScalar4Display.LookupTable = treesLUT\n")
		my_file.write("warpByScalar4Display.OSPRayScaleArray = 'trees'\n")
		my_file.write("warpByScalar4Display.OSPRayScaleFunction = 'PiecewiseFunction'\n")
		my_file.write("warpByScalar4Display.GlyphType = 'Arrow'\n")
		my_file.write("warpByScalar4Display.ScalarOpacityUnitDistance = 1.3377148308615403\n")

		# hide data in view
		my_file.write("Hide(id_forest0vtk, renderView1)\n")

		# show color bar/color legend
		my_file.write("warpByScalar4Display.SetScalarBarVisibility(renderView1, True)\n")

		# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
		my_file.write("treesLUT.ApplyPreset('PiYG', True)\n")

		# Properties modified on treesLUT
		my_file.write("treesLUT.EnableOpacityMapping = 1\n")

		# Properties modified on treesPWF
		my_file.write("treesPWF.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 0.7039473652839661, 0.5, 0.0]\n")
		my_file.close()
