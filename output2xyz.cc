///
/// This file is part of Shalava.
///
/// Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
///
/// Shalava is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Shalava is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Shalava; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "write_output_xyz.icc"

int main (int argc, char **argv) {
	environment rheolef(argc,argv);
	if (argc == 1) {
		derr << "usage: " << argv[0] << "mesh approx[P1] xe ye south west reso iteration" << endl;
		exit (0);
	}
	geo omega (argv[1]);
	string approx             = (argc > 2) ? argv[2] : "P1";
	Float xe                  = (argc > 3) ? atof(argv[3]) : 0.;
	Float ye                  = (argc > 4) ? atof(argv[4]) : 0.;
	Float south               = (argc > 5) ? atof(argv[5]) : 0.;
	Float west                = (argc > 6) ? atof(argv[6]) : 0.;
	Float reso                = (argc > 7) ? atof(argv[7]) : 20.;
	size_t iteration	  = (argc > 8) ? atoi(argv[8]) : 0;
	Float L = 100.;
	space Xh  (omega, approx);
	branch get("t","time", "h", "f+h");
	Float t=0;
	field time;
	field h;
	field fph;
	string name_branch = "output-"+itos(iteration)+".branch.gz";
 	idiststream in_h (name_branch);
	in_h >> get (t, time, h, fph);
	in_h.close();
	field f_topo;
	idiststream in_f ("fh_smooth.field");
	in_f >> f_topo;
	h = interpolate(Xh,h);  //z deja dimensionné
	f_topo = interpolate(Xh,f_topo);  //z adim
	fph = interpolate(Xh,fph);
	fph=L*f_topo+h;
	derr << "time = " << t << endl;
	Float xmin_adim = omega.xmin()[0];
	Float ymin_adim = omega.xmin()[1];
	Float xmax_adim = omega.xmax()[0];
	Float ymax_adim = omega.xmax()[1];
	write_xyz("DEM_fph", fph, xmin_adim, ymin_adim, xmax_adim, ymax_adim, xe, ye, south, west, L, reso, iteration);
	write_xyz("DEM_h", h, xmin_adim, ymin_adim, xmax_adim, ymax_adim, xe, ye, south, west, L, reso, iteration);
}
