#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
from tkinter import *
from tkinter import ttk
from vertical_canvas import *
from lavaview_variable import *
from hot_spot_Frame import *

class page4():
	"""
	A class to create the first page about the domain
	"""
	def __init__(self, parent, mytitle, *args, **kw):
		self.parent=parent
		f = parent.n.add(mytitle)
		parent.n.tab(mytitle).configure(background=dark_grey,fg="white",state="disabled")
		self.scrollable_canvas  = VerticalScrolledFrame(f)
		self.scrollable_canvas.pack(fill = 'both', expand = 1)
		parent.n.tab(mytitle).bind('<1>',self.reset_the_view,add="+") #reset the view after click on tabs
		main_window = self.scrollable_canvas.interior
		self.forest_characteristics_Frame = Lavaview_forest_Frame(main_window,self)
		self.forest_characteristics_Frame.pack(padx = 5, pady = 5)
		self.forest_characteristics_Frame.configure(borderwidth=0, bg=dark_grey, fg= "white")
		self.forest_characteristics_Frame.regions_frame.configure(bg=light_grey, highlightthickness=2)
		Label(self.forest_characteristics_Frame.regions_frame,text="Regions setting", bg=light_grey, fg="white", font=("Helvetica", 12, "bold")).grid(row=0,column=0,padx = 5, pady = 5)
		Frame_for_button=Frame(main_window,bg=dark_grey)
		Frame_for_button.pack(padx = 5, pady = 5)
		previous_from_4_to_3 = ttk.Button(Frame_for_button, text = 'Previous', command = lambda:parent.n.previouspage())
		next_from_3_to_4 = ttk.Button(Frame_for_button, text = 'Next', command = lambda:parent.n.nextpage())
		previous_from_4_to_3.grid(row=0,column=0, padx = 5, pady = 25)
		next_from_3_to_4.grid(row=0,column=1, padx = 5, pady = 25)
	def reset_the_view(self, event):
		self.scrollable_canvas.canvas.xview_moveto(0)
		self.scrollable_canvas.canvas.yview_moveto(0)
