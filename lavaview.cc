///
/// This file is part of Shalava.
///
/// Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
///
/// Shalava is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Shalava is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Shalava; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
#include <ctime>
using namespace rheolef;
using namespace std;
#include "lavaview.h"
#include "invert_h.icc"
#include "variable_flow_rate.icc"
#include "read_shape_input.icc"

struct profile { 
	Float operator() (const point& x) const {
		point xe;
		xe[0] = xe_0;
		xe[1] = xe_1;
		Float r = norm(x-xe);
		if ( (r<=radius) ) return sqr(radius)-sqr(r); //( sqr(radius)/exp(-1.) )*exp( -1./(-1.-sqr(r/radius)) );
		return 0.;
	}
	profile (const Float& xe_01, const Float& xe_11, const Float& radius1) : xe_0(xe_01), xe_1(xe_11), radius(radius1){}
	Float xe_0; Float xe_1; Float radius;
};

Float id_ti_tf(Float t, Float ti, Float tf){
	if ( (t>=ti) && (t<=tf) ) return 1.;
	return 0.;
}

point space_adim(point X, point Xe, Float west, Float south, Float L){
	Float xmin=Xe[0]-west;
	Float ymin=Xe[1]-south;
	point X_adim;
	X_adim[0] = (X[0]-xmin)/L;
	X_adim[1] = (X[1]-ymin)/L;
	return X_adim;	
}

struct positif : std::unary_function<Float,Float> {
	Float operator() (const Float& h) const { if (h < 0) return 0.; else return h; }
};

struct id_hp: unary_function<Float,Float> {
	Float operator() (const Float& h) const { 
		if ( h > 1e-8 )  return 0;
		return 0.;}
};


field criteria ( field h0, field h1, field h2, field w)  { return h0 + h1 + h2 + w; }

struct max_one: unary_function<Float,Float> {
	Float operator() (const Float& id) const { 
		return  min(id,1.);}
};
/*struct pnpoly { 
	Float operator() (const point& X) const {
		size_t i, j, c = 0;
		for (i = 1, j = npol; i <= npol; j = i++) {
			if (   ( ((point(v[i])[1]<=X[1]) && (X[1]<point(v[j])[1])) 
			  ||     ((point(v[j])[1]<=X[1]) && (X[1]<point(v[i])[1])) ) && ( (point(v[i])[0]<=X[0]) || (point(v[j])[0]<=X[0])  ) ){
				if (point(v[i])[0]+(X[1]-point(v[i])[1])/(point(v[j])[1]-point(v[i])[1])*(point(v[j])[0]-point(v[i])[0])<X[0])
				{c = !c;}
			}
		}
		return c;
	}
	pnpoly (const size_t& npol1, vector<point> v1) : npol(npol1), v(v1){}
	size_t npol; vector<point> v;
};*/

struct pnpoly { 
	Float operator() (const point& X) const {
		size_t i, j, c = 0;
		for (i = 1, j = npol; i <= npol; j = i++) {
			if (   ( ((point(v[i])[1]<=X[1]) && (X[1]<point(v[j])[1])) 
			  ||     ((point(v[j])[1]<=X[1]) && (X[1]<point(v[i])[1])) ) && ( (point(v[i])[0]<=X[0]) || (point(v[j])[0]<=X[0])  ) ){
				if (point(v[i])[0]+(X[1]-point(v[i])[1])/(point(v[j])[1]-point(v[i])[1])*(point(v[j])[0]-point(v[i])[0])<X[0])
				{c = !c;}
			}
		}
		return c;
	}
	pnpoly (const size_t& npol1, vector<point> v1) : npol(npol1), v(v1){}
	size_t npol; vector<point> v;
};

void update_flow_rate(field& ws, vector<Float>& Q, Float& total_flow_rate, const size_t& nb_vents, const vector<point>& xe, const vector<Float>& R, const Float& west, const Float& south, const Float& L, const Float& U, const Float& T_carac, const vector<string>& flow_rate_filenames, const Float& timer, const Float& delta_t,  const vector<Float>& ti, const vector<Float>& tf){
	for (size_t i=1; i <= nb_vents; i++){
		Float xe_0 = space_adim(point(xe[i]),point(xe[1]),west,south,L)[0];
		Float xe_1 = space_adim(point(xe[i]),point(xe[1]),west,south,L)[1];
		Float ti_adim = ti[i]/T_carac;
		Float tf_adim = tf[i]/T_carac;
		if (flow_rate_filenames[i]=="none") {
			Q[i] = id_ti_tf(timer,ti_adim,tf_adim)*Q[i];
		}
		else{
			Q[i]= Q_t((timer+ delta_t)*T_carac,flow_rate_filenames[i]);
		}
		Float pi = acos(-1.0);
		Float lambda_source = sqr(L)*2.*Q[i]/(U*pi*pow(R[i],4));
		Float R_adim = R[i]/L;
		space Xh = ws.get_space();
		ws += lambda_source*interpolate(Xh,profile(xe_0,xe_1,R_adim));
		total_flow_rate += Q[i];
	}
}

void build_forest(field& id_forest, field& Bi_star_h, field& Dam1_h, const size_t& nb_regions, const vector<size_t>& nb_vertex, const vector<vector<point>>& vertex, const Float& Bi_star, const Float& r_darcy, const string& presence_of_trees, const vector<point>& xe, const Float& west, const Float& south, const Float& L){
	if (presence_of_trees == "region"){
		space Xh = id_forest.get_space();
		id_forest = field(Xh, 0.);
		for (size_t i=1; i <= nb_regions; i++){
			size_t nb_vertex_i = nb_vertex[i];
			vector<point> vertex_i(nb_vertex_i+1);
			for (size_t j=1; j <= nb_vertex_i; j++){
				vertex_i[j]=space_adim(point(vertex[i][j]),point(xe[1]),west,south,L);
			}
			id_forest += interpolate(Xh,pnpoly(nb_vertex_i,vertex_i));
		}
		id_forest = interpolate(Xh,compose(max_one(),id_forest));
		Bi_star_h = Bi_star*id_forest;
		Dam1_h = sqr(r_darcy)*id_forest;
	}
}

void build_shape(field& id_shape, size_t& nb_shape_vertex, vector<point>& shape_vertex, const vector<point>& xe, const Float& west, const Float& south, const Float& L, const string& final_shape){
	if (final_shape != "none"){
		derr << "making id_shape.branch" << endl;
		get_shape(nb_shape_vertex,shape_vertex);
		vector<point> shape_vertex_adim(nb_shape_vertex+1,point(0.,0.));
		for (size_t i = 1; i <= nb_shape_vertex; i++) {
			shape_vertex_adim[i] = space_adim(point(shape_vertex[i]),point(xe[1]),west,south,L);
		}
	space Xh = id_shape.get_space();
	id_shape = interpolate(Xh,pnpoly(nb_shape_vertex,shape_vertex_adim));	
	}
}

/*void save_state(){
	odiststream o_h ( "last.field" );  // ( "h-"+itos(k)+".field" );
	o_h	<< catchmark("time(s)")			<< timer << endl
		<< catchmark("delta_t")			<< delta_t << endl
		<< catchmark("delta_t_prec")	<< delta_t_prec << endl
		<< catchmark("k")				<< k+1 << endl
		<< catchmark("h_old")			<< h_old
		<< catchmark("h")				<< h ;
	o_h.close();
}*/

int main (int argc, char **argv) {
	environment rheolef(argc,argv);
	if (argc == 1) {
		derr << "usage: " << argv[0] << " mesh approx[P1] tf(s)[50000] ad_max[1](adapt maillage) hmax[1] hration[0] nbv[1e5]" << endl;
		exit (0);
	}
	adapt_option_type options;
	time_t tbegin,tend;
	double texec=0.;
	tbegin=time(NULL);              // get the current calendar time
	#include "input.h"
	#include "read_input.icc"
	#include "print_input.icc"
	ofstream objetfichier("data.log");
	objetfichier << "# k " << " simulation_time(s) " << " computing_time(s) " << " delta_t " << "|dh_dt|L2 " << "memory " << "numerical_volume " << "theorical_volume" << " ration_vol_neg " << endl;
	Float g       = 9.81;
	Float L       = 100.;                   // caracteristic length (m)
	options.hmin = reso/L;
	geo omega (argv[1]);
	string approx             = (argc > 2) ?      argv[2]  : "P1";
	Float tf_dim              = (argc > 3) ? atof(argv[3]) : 50000.;          // final time (s)
	size_t ad_max             = (argc > 4) ? atoi(argv[4]) : 1;               // max iter mesh-adapt loop
	//options.hcoef           = (argc > 5) ? atof(argv[5]) : 1.1;
	options.hmax              = (argc > 6) ? atof(argv[6]) : 2;
	options.ratio             = (argc > 7) ? atof(argv[7]) : 1.5;
	options.n_vertices_max    = (argc > 8) ? atof(argv[8]) : 1e6;
	Float Bi      = tau/(rho*g*L);          // Bingham number
	Float pi      = acos(-1.0);
	Float T_carac = mu0/(rho*g*L);          // caracteristic gravity time (s)
	Float U       = rho*g*pow(L,3)/(mu0*L); // caracteristic horizontal velocity
	Float epsilon = 1e-12;                  // tolerance residu dh/dt
	Float tol     = 1e-12;                  // tolerance Newton
	//Float X = M/d -1.;                    // ratio diameter cylinder/space between cylinders
	Float a = pi/4.;                        // square config a = pi/4.; hexagonal config a = pi/(2*sqrt(3));
	if (arrangement_of_trees == "hexagonal") {a = pi/(2*sqrt(3));}
	Float porosity = 1. - a*sqr(d/M);       // sqr(X+1); porosity = volumic solid fraction; fluid fraction = 1 - solid fraction
	Float kappa = sqr(d)*0.16*a*pow((1-sqrt((1-porosity)/a)),3)/((1-porosity)*sqrt(porosity)); // Tamayol-Bahrami law (2010)
	Float Da = kappa/sqr(L);                 // Darcy number
	Float alpha_vas = 1.5;
	Float r_darcy = 1./sqrt(Da);
	Float Bi_star = (sqrt(porosity)/alpha_vas)*(Bi/r_darcy);   //Bingham number in porous media
	if (presence_of_trees == "no") {
		derr << "simulation with no trees" << endl;
		r_darcy = 0.;
		Bi_star = 0.;
	}
	derr << "Q[1] = " << Q[1] << endl;
	Float delta_t0_dim = 0.05*pow(mu0*pow(R[1],6)/(rho*g*pow(Q[1],3)),0.25); //delta_t auto (s)  sqr(L)*   0.25*
	Float delta_t0 = delta_t0_dim/T_carac;
	Float delta_t = delta_t0;
	Float delta_t_prec = delta_t;
	Float theta = 1.05;  //factor of geometrical increase 1.5
	Float tf_adim =tf_dim/T_carac;
	derr	<< "# lavaview:" << endl
			<< "# geo = "                     << omega.name() << endl
			<< "# delta_t (s) = "             << delta_t0_dim << endl
			<< "# ad_max (adapt maillage) = " << ad_max       << endl
			<< "# T caract = "                << T_carac      << endl
			<< "# Bi = "                      << Bi           << endl
			<< "# U = "                       << U            << endl
			<< "# porosity = "                << porosity     << endl
			<< "# permeability = "            << kappa        << endl
			<< "# r_darcy = "                 << r_darcy      << endl
			<< "# Bi_star = "                 << Bi_star      << endl
			<< "# delta_t auto (s) = "        << delta_t0_dim << endl;
	size_t k_max    = 1e6;    // nb of time-step max
	size_t max_iter = 50;    // max_iter newton
	Float timer = 0.;         // dimensionless time
	int status = 1.;
	space Xh  (omega, approx);
	field ws(Xh,0.);
	Float time_eruption_dim = 0.;
	Float total_flow_rate = 0;
	update_flow_rate(ws, Q, total_flow_rate, nb_vents, xe, R, west, south, L, U, T_carac, flow_rate_filenames, timer, delta_t, ti, tf);
	field f_topo;
	idiststream in ("fh_smooth.field");
		in >> f_topo;
	field fh = interpolate(Xh,f_topo);
	field id_forest (Xh, 0.);
	field id_shape (Xh, 0.);
	field Dam1_h (Xh, 0.);
	field Bi_star_h (Xh, 0.);
//------------------ Forest ---------------------
	if (presence_of_trees == "region"){
		build_forest(id_forest, Bi_star_h, Dam1_h, nb_regions, nb_vertex, vertex, Bi_star, r_darcy, presence_of_trees, xe, west, south, L);
		odiststream id_forest_h ( "id_forest.branch" );
		branch vtk_forest ("t","trees","f");
		id_forest_h << vtk_forest (0., id_forest, fh);
		id_forest_h.close();
	}
//------------------ Shape ---------------------
	if (final_shape != "none"){
		build_shape(id_shape, nb_shape_vertex, shape_vertex, xe, west, south, L, final_shape);
		odiststream id_shape_h ( "id_shape.branch" );
		branch vtk_shape ("t","shape","f");
		id_shape_h << vtk_shape (0., id_shape, fh);
		id_shape_h.close();
	}
	omega = adapt (ws,options);
	omega.save();
	Xh   = space(omega, approx);
	space Xh2   = space(omega, approx, "vector");
	ws = field(Xh,0.);
	quadrature_option_type qopt;
	qopt.set_family(quadrature_option_type::gauss );
	qopt.set_order(2*Xh.degree()-1);
	quadrature_option_type qopt_gl;
	qopt_gl.set_family(quadrature_option_type::gauss_lobatto );
	qopt_gl.set_order(1);
	trial u(Xh);
	test v(Xh);
	form m = integrate(u*v,qopt_gl);
	solver m_fact (m.uu());

	Float time_eruption = time_eruption_dim/T_carac;
	fh = field(Xh);
	fh = interpolate(Xh,f_topo);
	field h (Xh,0.);
	field h_old  = h;
	field dh_dt (Xh,1.);
	Float norm_L2_dh_dt = 1.;
	field error_phi_h (Xh,1.);
	Float norm_L2_error_phi_h = 1.;
	Dam1_h = field (Xh, sqr(r_darcy));
	Bi_star_h = field (Xh, Bi_star);
	id_forest = field (Xh, 0.);
	build_forest(id_forest, Bi_star_h, Dam1_h, nb_regions, nb_vertex, vertex, Bi_star, r_darcy, presence_of_trees, xe, west, south, L);
	field time_h (Xh,timer); 
	derr	<< "# kmax = " << k_max << endl
		<< "# k ad time(s) |dh_dt|L2 " << endl;
	Float volume_theorique = 0.;
	for (size_t k = 0; k <= k_max+1; k++) {   // time loop
		timer = timer + delta_t;
		Float coeff1 = delta_t*(delta_t+delta_t_prec)/(2*delta_t+delta_t_prec);
		Float coeff2 = (delta_t+delta_t_prec)/(delta_t*delta_t_prec);
		Float coeff3 = -delta_t/((delta_t+delta_t_prec)*delta_t_prec);
		field phi = h;
		ws = field(Xh,0.);
		total_flow_rate = 0;
		update_flow_rate(ws, Q, total_flow_rate, nb_vents, xe, R, west, south, L, U, T_carac, flow_rate_filenames, timer, delta_t, ti, tf);
		for (size_t ad = 0 ; ad <= ad_max; ad++){ // mesh-adapt loop
			tend=time(NULL);                // get the current calendar time
			texec=difftime(tend,tbegin);    // tend-tbegin (result in second)
 			Float tol_ad = tol;
			size_t max_iter_ad =  max_iter;
			shallow_lava_HB F (Bi, Dam1_h, Bi_star_h, h, h_old, fh, field(ws), coeff1, coeff2, coeff3, Xh, Xh2);
			status = damped_newton (F, phi, tol_ad, max_iter_ad, &derr);
			derr << "tolerance getting after Newton alogrithm = " << tol_ad << endl;
			error_phi_h = phi - h;
			norm_L2_error_phi_h = sqrt(m(error_phi_h,error_phi_h));
			if (norm_L2_error_phi_h == 0. || tol_ad > tol ){
				phi = h;
				timer = timer - delta_t;
				delta_t = delta_t/2.;
				derr << "problem of convergence => delta_t = " << delta_t*T_carac << endl;
				timer = timer + delta_t;
				coeff1 = delta_t*(delta_t+delta_t_prec)/(2*delta_t+delta_t_prec);
				coeff2 = (delta_t+delta_t_prec)/(delta_t*delta_t_prec);
				coeff3 = -delta_t/((delta_t+delta_t_prec)*delta_t_prec);
				ad = 0;
			}
			else{
				//phi = interpolate (Xh, compose(positif(),phi));
				if (ad==ad_max || (k%5!=0 && k>=2) ) break;
				derr << " begin adapt" << endl;
				field ch = criteria(phi,h,h_old, 10.*delta_t*ws);
				omega = adapt (ch,options);
				omega.save();
				Xh   = space(omega, approx);
				Xh2  = space(omega, approx, "vector");
				ws = field(Xh,0.);
				total_flow_rate = 0;
				update_flow_rate(ws, Q, total_flow_rate, nb_vents, xe, R, west, south, L, U, T_carac, flow_rate_filenames, timer, delta_t, ti, tf);
				//Float flux_rate_i = U*sqr(L)*integrate(omega, ws_i, qopt_gl);
				//Float flux_rate_i_theoric = Q[i];
				//if ( (timer >= ti_adim) && (timer <= tf_adim) ) {ws_i = (flux_rate_i_theoric/flux_rate_i)*ws_i;}
				//ws += ws_i;
				u = trial (Xh);
				v = test(Xh);
				m = integrate(u*v);
				m_fact = solver (m.uu());
				fh = interpolate(Xh,f_topo);
				h     = interpolate(Xh,h);
				h_old = interpolate(Xh,h_old);
				phi   = interpolate(Xh,phi);
				dh_dt = interpolate(Xh,dh_dt);
				error_phi_h = interpolate(Xh,error_phi_h);
				Dam1_h = field(Xh, sqr(r_darcy));
				Bi_star_h = field(Xh, Bi_star);
				id_forest = field (Xh, 0.);
				build_forest(id_forest, Bi_star_h, Dam1_h, nb_regions, nb_vertex, vertex, Bi_star, r_darcy, presence_of_trees, xe, west, south, L);
			}
		}
	dh_dt = (phi/coeff1) - coeff2*h - coeff3*h_old;
	norm_L2_dh_dt = sqrt(m(dh_dt,dh_dt));
	h_old=h;
	Float volume_negatif = pow(L,3)*integrate(omega, min(phi,0.), qopt_gl);  //volume in litre
	Float volume_litre = pow(L,3)*integrate(omega, phi, qopt_gl);  //volume in litre
	Float ratio_vol_neg = 100.*abs(volume_negatif/volume_litre); //%
	Float volume_double = pow(L,3)*integrate(omega, sqr(phi), qopt_gl);  //volume in litre
	volume_theorique += total_flow_rate*delta_t*T_carac;
	//field id_h = interpolate (Xh, compose(id_hp(),h));
	//Float surface = sqr(L)*integrate(omega, id_h, qopt_gl);
	//h = phi*(1.+(-1.+volume_theorique/volume_litre) );
	//h = phi;
	h = interpolate(Xh,  phi*(1.+(volume_litre*phi/volume_double)*(-1.+volume_theorique/volume_litre) )  );
	//save_state();
	delta_t_prec = delta_t;
	if (delta_t < delta_t0) {delta_t = theta*delta_t;} //timer > time_eruption || 
	else if (delta_t*T_carac<=60) {delta_t = 1.01*delta_t;}
	objetfichier << k << " " << timer*T_carac << " " << texec << " " << delta_t*T_carac << " " << norm_L2_dh_dt << " " << 100*dis_memory_usage()/memory_size()  << " " << volume_litre << " " << volume_theorique << " " << ratio_vol_neg << endl;
	//volume_litre << " " << texec << " " << delta_t*T_carac
	time_h = field(Xh,timer*T_carac);
	//field grad_fph_P1 = interpolate(Xh2,grad(fh+h));
	//field u_parallel = U*interpolate(Xh2,(-1.)*compose(invert(Bi),h,norm(grad(fh+h)),Dam1_h,Bi_star_h)*compose(mu(Bi),h,norm(grad(fh+h)),Dam1_h,Bi_star_h)*grad(fh+h)); //
	field h_plus = interpolate(Xh,max(0.,h));
	odiststream ovtk ("output-"+itos(k)+".branch");
	branch vtk_event ("t","time","h", "f+h"); //"f","u_parallel"
	ovtk << vtk_event (timer*T_carac, time_h, L*h_plus, fh+h_plus); //,fh,fh+h,u_parallel
	if (k == k_max) break;
	if (timer > tf_adim) break;
	//if (norm_L2_dh_dt < epsilon) break;
	}
}
