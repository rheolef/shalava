#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
PKGDATADIR="."
from tkinter import *
import tkinter.filedialog
import functools
import matplotlib
import matplotlib.pyplot as plt
import os
import webbrowser
import utm
import json
import kml2geojson

import importlib.machinery
lavaview_variable = importlib.machinery.SourceFileLoader('lavaview_variable',PKGDATADIR+'/lavaview_variable.py').load_module()
from lavaview_variable import *

class Lavaview_vent_Frame(LabelFrame):
	def __init__(self, parent, vent_nb, row_value=0, column_value=0, *args, **kw):
		LabelFrame.__init__(self, parent, *args, **kw)
		self.parent=parent
		self.vent_nb_variable = IntVar()
		self.vent_nb_variable.set(vent_nb)
		self.configure(text="vent "+str(self.vent_nb_variable.get()),bg=dark_grey,fg= "white")
		self.grid(sticky="W", row = row_value, column = column_value)
		self.xe_variable = Lavaview_scale_variable(self,0,1000,1,2,"xe","abscissa",0,0,0,dark_grey)
		self.ye_variable = Lavaview_scale_variable(self,0,1000,1,2,"ye","ordinate",0,1,0,dark_grey)
		self.xe_variable.Myvar.trace("w", self.callback_trace_xe)
		self.ye_variable.Myvar.trace("w", self.callback_trace_ye)
		self.xe_variable.Myscale.config(command=self.callback_scale_xe)
		self.ye_variable.Myscale.config(command=self.callback_scale_ye)
		self.xe_variable.MyEntry.bind('<FocusOut>', functools.partial(self.controle_value, variable=self.xe_variable))
		self.ye_variable.MyEntry.bind('<FocusOut>', functools.partial(self.controle_value, variable=self.ye_variable))
		Label(self, text = "Flow rate: ", bg=dark_grey, fg= "white").grid(row = 2, column = 0)
		self.varQ = StringVar() #variable = Constant or Variable
		self.constant_Radio = Radiobutton(self, variable=self.varQ, text = "Constant", value=1, bg=dark_grey, fg= "white", selectcolor="#9F9F98", highlightthickness=0, command = lambda:self.command_for_constant())
		self.variable_Radio = Radiobutton(self, variable=self.varQ, text = "Variable", value=2, bg=dark_grey, fg= "white", selectcolor="#9F9F98", highlightthickness=0, command = lambda:self.command_for_variable())
		self.constant_Radio.grid(row = 2, column = 1, padx = 5, pady = 5)
		self.variable_Radio.grid(row = 2, column = 2, padx = 5, pady = 5)
		self.flow_rate_Frame = Frame(self,bg=light_grey, highlightthickness=2)
		Label(self.flow_rate_Frame, text="Flow rate setting", bg=light_grey, fg="white", font=("Helvetica", 12, "bold")).grid(row=0, column=0, columnspan = 5, padx = 5, pady = 5)
		self.Q_variable = Lavaview_variable(self.flow_rate_Frame,"Q","rate of flow (m3/s)",100,1,0,light_grey)
		self.R_variable = Lavaview_variable(self.flow_rate_Frame,"R","radius",20,1,2,light_grey)
		self.ti_variable = Lavaview_variable(self.flow_rate_Frame,"ti","initial time",0,2,0,light_grey)
		self.tf_variable = Lavaview_variable(self.flow_rate_Frame,"tf","final time",10000,2,2,light_grey)
		self.load_file = PhotoImage(file=PKGDATADIR+'/load_file_icone_36px.png')
		self.load_flow_rate_button = Button(self.flow_rate_Frame,text="Load flow rate file", image=self.load_file, borderwidth=0, highlightthickness=0, command=self.open_flow_rate_file) #, background=light_grey,
		self.flow_rate_filename="none"
		self.short_filename_Var = StringVar()
		self.short_filename_Var.set("none")
		self.variable_filename_entry = Entry(self.flow_rate_Frame,textvariable=self.short_filename_Var, width=10, font=("Helvetica", 16),state="disabled")
		self.Q_curve = PhotoImage()
		self.Q_canvas = Canvas(self.flow_rate_Frame, width=0, height=0, bg=light_grey,bd=0,highlightthickness=0)
		self.icone_rm = PhotoImage(file=PKGDATADIR+'/cross.gif')
		self.rm_vent_button = Button(self, text = 'Remove this vent', image=self.icone_rm, command=lambda:self.command_rm_button(parent))
		if self.vent_nb_variable.get()>1:
			self.rm_vent_button.grid(row = 4, column = 0)
		self.constant_Radio.select()
		self.command_for_constant()
	def command_rm_button(self,parent):
		self.grid_forget()
		del parent.vent_frame_list[self.vent_nb_variable.get()-1]
		parent.decrement_nb_vents()
		parent.update_vent_nbs()

	def open_flow_rate_file(self):
		filename=tkinter.filedialog.askopenfilename(title="Open flow rate file", parent=self.parent)
		if not filename: # askopenfilename return `false` if dialog closed with "cancel".
			return
		#f=open(filename,"r").splitlines()
		#lines = f.readlines()
		#f.close()
		try:
			with open(filename,"r") as f_in:
				lines = list(line for line in (l.strip() for l in f_in) if line)
			self.plot_Q_t(lines)
		except:
			messagebox.showerror(title="Error with flow rate file format", message="wrong flow rate format, use two columns file \"t(s) Q(t)(m3.s-1)\"", parent=self.parent)
		else:
			self.flow_rate_filename = filename
			self.short_filename_Var.set(filename.split("/")[-1])

	def plot_Q_t(self,lines):
		matplotlib.rc('axes',edgecolor='w')
		x=['0']
		y=['0']
		for line in lines:
			x.append(line.split()[0])
			y.append(line.split()[1])
		plt.figure(self.vent_nb_variable.get(),figsize=(3.5,2.5))
		plt.xlabel('time (s)').set_color("w")
		plt.ylabel('flow rate '+r'$m^3.s^{-1}$').set_color("w")
		plt.ticklabel_format(axis='x', style='sci', scilimits=(0, 4),useMathText='True')
		plt.tick_params(axis='both', colors='w')
		plt.plot(x,y,color='#00B2FF')
		os.makedirs('tmp', exist_ok=True)
		plt.savefig('tmp/curve_tmp.png',bbox_inches='tight', transparent = True)
		self.Q_curve = PhotoImage(file='tmp/curve_tmp.png')
		self.Q_canvas.config(width=self.Q_curve.width(), height=self.Q_curve.height())
		self.Q_canvas.create_image(0, 0, anchor=NW, image=self.Q_curve)
		self.Q_canvas.grid(row=2, column=0, columnspan=5, padx = 5, pady = 5)
		plt.figure(self.vent_nb_variable.get()).clear()

	def open_constant(self):
		self.flow_rate_filename = "none"
		self.short_filename_Var.set("none")
		self.flow_rate_Frame.grid(row=3, column=0, columnspan= 3, padx = 5, pady = 5)
		self.Q_variable.MyEntry.grid(row=1, column=1, padx = 5, pady = 5)
		self.Q_variable.MyLabel.configure(text="Q :")
		self.ti_variable.MyEntry.grid(row=2, column=1, padx = 5, pady = 5)
		self.ti_variable.MyLabel.grid(row=2, column=0, padx = 5, pady = 5)
		self.tf_variable.MyEntry.grid(row=2, column=3, padx = 5, pady = 5)
		self.tf_variable.MyLabel.grid(row=2, column=2, padx = 5, pady = 5)
	def create_variable_button(self):
		self.load_flow_rate_button.grid(row=1, column=2, padx = (0,5), pady = 5)
	def create_variable_filename(self):
		self.variable_filename_entry.grid(row=1, column=1, padx = (5,0), pady = 5)
	def move_R_variable_for_variable(self):
		self.R_variable.MyLabel.grid(row=1,column=3, padx = 5, pady = 5)
		self.R_variable.MyEntry.grid(row=1,column=4, padx = 5, pady = 5)
	def move_R_variable_for_constant(self):
		self.R_variable.MyLabel.grid(row=1,column=2, padx = 5, pady = 5)
		self.R_variable.MyEntry.grid(row=1,column=3, padx = 5, pady = 5)
	def close_constant(self):
		self.Q_variable.MyEntry.grid_forget()
		self.ti_variable.MyEntry.grid_forget()
		self.ti_variable.MyLabel.grid_forget()
		self.tf_variable.MyEntry.grid_forget()
		self.tf_variable.MyLabel.grid_forget()
	def close_variable(self):
		self.load_flow_rate_button.grid_forget()
		self.variable_filename_entry.grid_forget()
		self.Q_canvas.config(width=0, height=0) # or add tag to image and delete it
	def command_for_constant(self):
		self.close_variable()
		self.open_constant()
		self.move_R_variable_for_constant()
	def command_for_variable(self):
		self.open_constant()
		self.Q_variable.MyLabel.configure(text="Q(t) :")
		self.close_constant()
		self.create_variable_button()
		self.create_variable_filename()
		self.move_R_variable_for_variable()
		self.Q_canvas.grid(row=2, column=0, columnspan=5, padx = 5, pady = 5)

	def inside_callback_trace(self,variable):
		if (variable.Myvar.get()!=""):
			try:
				variable.Myscalevar.set(float(variable.Myvar.get()))
			except:
				variable.Myvar.set(str(variable.Myscalevar.get()))
	def controle_value(self,event=None,variable=None):
		if (event!='mouv_scale') and (float(variable.Myvar.get())==variable.Myscalevar.get()):
			return False
		else:
			variable.Myvar.set(str(variable.Myscalevar.get()))
			return True
	def callback_trace_xe(self,*args):
		self.inside_callback_trace(self.xe_variable)
	def callback_scale_xe(self,*args):
		self.controle_value('mouv_scale',self.xe_variable)
	def callback_trace_ye(self,*args):
		self.inside_callback_trace(self.ye_variable)
	def callback_scale_ye(self,*args):
		self.controle_value('mouv_scale',self.ye_variable)

class Lavaview_region_Frame(LabelFrame):
	def __init__(self, parent, vent_nb, row_value=0, column_value=0, *args, **kw):
		LabelFrame.__init__(self, parent, *args, **kw)
		self.parent = parent
		self.vent_nb_variable = IntVar()
		self.vent_nb_variable.set(vent_nb)
		self.configure(text="region "+str(self.vent_nb_variable.get()), borderwidth=3, bg=light_grey, fg= "white")
		self.grid(sticky="W", row = row_value, column = column_value)
		self.Q_variable=Lavaview_hot_spot_Frame(self,"vertex",light_grey)
		self.Q_variable.grid(row = 2, column = 0)
		self.icone_rm = PhotoImage(file=PKGDATADIR+'/cross.gif')
		self.rm_vent_button = Button(self, text = 'Remove this vent', image=self.icone_rm, command=lambda:self.command_rm_button(parent))
		if self.vent_nb_variable.get()>1:
			self.rm_vent_button.grid(sticky="E", row = 1, column = 0)
	def command_rm_button(self,parent):
		self.grid_forget()
		del parent.vent_frame_list[self.vent_nb_variable.get()-1]
		parent.decrement_nb_vents()
		parent.update_vent_nbs()
	def LoadRegion(self):
		filename=tkinter.filedialog.askopenfilename(title="Open Region shapefile", filetypes=[('geojson files','.geojson'),('kml files','.kml')], parent=self)
		if not filename: # askopenfilename return `false` if dialog closed with "cancel".
			return
		os.makedirs('tmp', exist_ok=True)
		short_filename = filename.split("/")[-1]
		if short_filename.split(".")[-1]=="kml":
			kml2geojson.main.convert(filename,output_dir="tmp")
			filename = "tmp/"+".".join(short_filename.split(".")[0:-1])+".geojson"
		with open(filename) as f:
			data = json.load(f)
		for feature in data['features']:
			list_of_coordinates = feature['geometry']['coordinates']
			if len(list_of_coordinates)==1: #from GeoJson,list_of_coordinates=[[coordinate]] 1 hook too much
				list_of_coordinates = list_of_coordinates[0]
			del list_of_coordinates[-1]
			nb_vertex_input_value = len(list_of_coordinates)
			self.addrmRegion(nb_vertex_input_value)
			vertex_nb = 0
			for vertex in list_of_coordinates:
					utm_convert = utm.from_latlon(vertex[1],vertex[0])
					print('xe = '+str(utm_convert[0])+' ye = '+str(utm_convert[1]))
					self.allotVertexValue(utm_convert[0],utm_convert[1],vertex_nb)
					vertex_nb+=1
			break
	def addrmRegion(self,nb_vertex_input_value):
		nb_vertex_difference = nb_vertex_input_value - self.Q_variable.nb_vents.get()
		if nb_vertex_difference>0:
			for k in range(nb_vertex_difference):
				self.Q_variable.command_button()
		if nb_vertex_difference<0:
			for k in range(-nb_vertex_difference):
				self.Q_variable.vent_frame_list[-1].command_rm_button(self.Q_variable)
	def allotVertexValue(self,xe_value,ye_value, vertex_nb):
		self.Q_variable.vent_frame_list[vertex_nb].xe_vertex_scale.Myscalevar.set(xe_value)
		self.Q_variable.vent_frame_list[vertex_nb].ye_vertex_scale.Myscalevar.set(ye_value)

class Lavaview_vertex_Frame(Frame):
	def __init__(self, parent, vent_nb, row_value=0, column_value=0, *args, **kw):
		Frame.__init__(self, parent, *args, **kw)
		self.vent_nb_variable = IntVar()
		self.vent_nb_variable.set(vent_nb)
		self.configure(bg=light_grey)
		self.grid(sticky="W", row = row_value, column = column_value)
		self.xe_vertex_scale = Lavaview_vertical_scale_variable(self,"xe", 0, 0, 1, light_grey, 0,0, 1)
		self.ye_vertex_scale = Lavaview_vertical_scale_variable(self,"ye", 0, 0, 2, light_grey, 0,0, 1)
		self.Label_vertex_nb = Label(self,text="vertex "+str(self.vent_nb_variable.get())+" (UTM - E,N)", bg=light_grey, fg= "white")
		self.Label_vertex_nb.grid(row = 0, column = 0, rowspan = 2, padx = 5, pady = 5)
		self.xe_vertex_scale.Myvar.trace("w", self.callback_trace_xe_vertex)
		self.ye_vertex_scale.Myvar.trace("w", self.callback_trace_ye_vertex)
		self.xe_vertex_scale.Myscale.config(command=self.callback_scale_xe_vertex)
		self.ye_vertex_scale.Myscale.config(command=self.callback_scale_ye_vertex)
		self.xe_vertex_scale.MyEntry.bind('<FocusOut>', functools.partial(self.controle_value, variable=self.xe_vertex_scale))
		self.ye_vertex_scale.MyEntry.bind('<FocusOut>', functools.partial(self.controle_value, variable=self.ye_vertex_scale))
		self.icone_rm = PhotoImage(file=PKGDATADIR+'/cross.gif')
		self.rm_vent_button = Button(self, text = 'Remove this region', image=self.icone_rm, command=lambda:self.command_rm_button(parent))
		if self.vent_nb_variable.get()>3:
			self.rm_vent_button.grid(row = 0, column = 3)



	def inside_callback_trace(self,variable):
		if (variable.Myvar.get()!=""):
			try:
				variable.Myscalevar.set(float(variable.Myvar.get()))
			except:
				variable.Myvar.set(str(variable.Myscalevar.get()))
	def controle_value(self,event=None,variable=None):
		if (event!='mouv_scale') and (float(variable.Myvar.get())==variable.Myscalevar.get()):
			return False
		else:
			variable.Myvar.set(str(variable.Myscalevar.get()))
			return True
	def callback_trace_xe_vertex(self,*args):
		self.inside_callback_trace(self.xe_vertex_scale)
	def callback_scale_xe_vertex(self,*args):
		self.controle_value('mouv_scale',self.xe_vertex_scale)
	def callback_trace_ye_vertex(self,*args):
		self.inside_callback_trace(self.ye_vertex_scale)
	def callback_scale_ye_vertex(self,*args):
		self.controle_value('mouv_scale',self.ye_vertex_scale)


	def command_rm_button(self,parent):
		self.grid_forget()
		del parent.vent_frame_list[self.vent_nb_variable.get()-1]
		parent.decrement_nb_vents()
		parent.update_vent_nbs()

class Lavaview_forest_Frame(Frame):
	def __init__(self, parent, page, *args, **kw):
		LabelFrame.__init__(self, parent, *args, **kw)
		self.parent = parent
		self.page = page
		Label(self, text = "Forest characteristics", bg=dark_grey, fg= "white", font=("Helvetica", 16, "bold")).grid(row = 0, column = 0, columnspan=4)
		Label(self, text = "Presence of forest: ", bg=dark_grey, fg= "white").grid(row = 1, column = 0)
		self.varGr = StringVar()  #variable = No,Everywhere or Region
		self.no_Radio = Radiobutton(self, variable=self.varGr, text = "No", value=1, bg=dark_grey, fg= "white", selectcolor="#9F9F98", highlightthickness=0, command = lambda:self.command_for_no())
		self.everywhere_Radio = Radiobutton(self, variable=self.varGr, text = "Everywhere", value=2, bg=dark_grey, fg= "white", selectcolor="#9F9F98", highlightthickness=0, command = lambda:self.command_for_everywhere())
		self.region_Radio = Radiobutton(self, variable=self.varGr, text = "Region", value=3, bg=dark_grey, fg= "white", selectcolor="#9F9F98", highlightthickness=0, command = lambda:self.command_for_region())
		self.no_Radio.grid(row = 1, column = 1, padx = 5, pady = 5)
		self.everywhere_Radio.grid(row = 1, column = 2, padx = 5, pady = 5)
		self.region_Radio.grid(row = 1, column = 3, padx = 5, pady = 5)
		self.no_Radio.select() #default value
		self.arrangement_var = StringVar() #when varGr=everywhere or region
		self.tree_Frame = Frame(self, bg=light_grey, highlightthickness=2)
		Label(self.tree_Frame, text="Tree configuration", bg=light_grey, fg="white", font=("Helvetica", 12, "bold")).grid(row=0, column=0, columnspan = 3, padx = 5, pady = 5)
		self.regions_frame = Lavaview_hot_spot_Frame(self,"region",light_grey)
		self.d_variable = Lavaview_variable(self.tree_Frame, "d", "diameter of trunk", 0.4, 1, 0, light_grey)
		self.M_variable = Lavaview_variable(self.tree_Frame, "M", "distance between trees", 2.4, 2, 0, light_grey)
		self.square_Radio = Radiobutton(self.tree_Frame, variable=self.arrangement_var, text = "Square", value=1, bg=light_grey, fg= "white", selectcolor="#9F9F98", highlightthickness=0)
		self.hexa_Radio = Radiobutton(self.tree_Frame, variable=self.arrangement_var, text = "Hexagonal", value=2, bg=light_grey, fg= "white", selectcolor="#9F9F98", highlightthickness=0)
		self.square_Radio.select()
	def open_tree_frame(self):
		self.tree_Frame.grid(row = 2, column = 2, padx = 5, pady = 5)
		#self.tree_Frame.configure(borderwidth=4)
		Label(self.tree_Frame, text = "Tree packing:", bg=light_grey, fg= "white").grid(row = 3, column = 0)
		self.square_Radio.grid(row = 3, column = 1, padx = 5, pady = 5)
		self.hexa_Radio.grid(row = 3, column = 2, padx = 5, pady = 5)
	def close_tree_frame(self):
		self.tree_Frame.grid_forget()
	def close_regions_frame(self):
		self.regions_frame.grid_forget()
	def open_regions_frame(self):
		self.regions_frame.grid(row = 3, column = 2, padx = 5, pady = 5)
		self.regions_frame.configure(borderwidth=4)
	def command_for_no(self):
		self.close_tree_frame()
		self.close_regions_frame()
	def command_for_everywhere(self):
		self.open_tree_frame()
		self.close_regions_frame()
	def command_for_region(self):
		self.open_tree_frame()
		self.open_regions_frame()

class Lavaview_hot_spot_Frame(Frame):
	def __init__(self, parent, vent_or_region_or_vertex="vent", bg_color = dark_grey, *args, **kw):
		Frame.__init__(self, parent, *args, **kw)
		self.parent = parent
		self.configure(borderwidth=0, bg=bg_color)
		self.vent_region_vertex = vent_or_region_or_vertex
		self.nb_vents = IntVar()
		self.nb_vents.set(0)
		Label(self, text = "nb_"+self.vent_region_vertex+"s =", bg=bg_color, fg= "white").grid(sticky="E",row = 0, column = 1, pady = 5)
		Label(self, textvariable=self.nb_vents, bg=bg_color, fg= "white").grid(sticky="W", row = 0, column = 2, pady = 5)
		self.vent_frame_list = list()
		self.add_vent_button = Button(self, text = 'Add a '+self.vent_region_vertex, command=lambda:self.command_button())
		self.load_forest_frame = Frame(self,bg=light_grey,)
		self.load_file = PhotoImage(file=PKGDATADIR+'/load_file_icone_36px.png')
		self.forest_name_Var = StringVar()
		self.forest_name_Var.set("Load region")
		self.load_forest_entry = Entry(self.load_forest_frame,textvariable=self.forest_name_Var, width=10, font=("Helvetica", 16),state="disabled")
		self.load_forest_button = Button(self.load_forest_frame,text="Load a region shapefile", image=self.load_file, borderwidth=0, highlightthickness=0)
		self.load_forest_entry.grid(row=0, column=0, padx = (5,0), pady = 5)
		self.load_forest_button.grid(row=0, column=1, padx = (0,5), pady = 5)
		self.draw_region_button = Button(self, text="Draw region", command=self.drawPolygone)
		self.command_button()
		if (self.vent_region_vertex=="vertex"):
			self.command_button()
			self.command_button()
	def drawPolygone(self):
		webbrowser.open('http://geojson.io/',new=0, autoraise=True)
	def update_scale(self,variable,min_value,max_value):  #best is to reuse this function from p1 (adding page in class)
		if (variable.min_scale_value.get()!=min_value) or (variable.max_scale_value.get()!=max_value):
			variable.min_scale_value.set(min_value)
			variable.max_scale_value.set(max_value)
			variable.my_interval = (max_value - min_value)/1.0
			variable.Myscale.config(from_=min_value, to=max_value,tickinterval=variable.my_interval)
			#if (float(variable.Myvar.get())>max_value) or (float(variable.Myvar.get())<min_value):
			variable.Myvar.set(str(0.5*(max_value+min_value)))
	def add_vent(self):
		if (self.vent_region_vertex=="vent"):
			new_vent = Lavaview_vent_Frame(self,self.nb_vents.get(),self.nb_vents.get(),0)
			if self.nb_vents.get()>1:
				xmin_value = self.vent_frame_list[0].xe_variable.min_scale_value.get()
				xmax_value = self.vent_frame_list[0].xe_variable.max_scale_value.get()
				ymin_value = self.vent_frame_list[0].ye_variable.min_scale_value.get()
				ymax_value = self.vent_frame_list[0].ye_variable.max_scale_value.get()
				self.update_scale(new_vent.xe_variable, xmin_value, xmax_value)
				self.update_scale(new_vent.ye_variable, ymin_value, ymax_value)
		elif(self.vent_region_vertex=="region"):
			new_vent = Lavaview_region_Frame(self,self.nb_vents.get(),self.nb_vents.get(),0)
		elif(self.vent_region_vertex=="vertex"):
			new_vent = Lavaview_vertex_Frame(self,self.nb_vents.get(),self.nb_vents.get(),0)
			xmin_value = self.parent.parent.parent.page.parent.p3.hot_spot_Frame.vent_frame_list[0].xe_variable.min_scale_value.get()
			ymin_value = self.parent.parent.parent.page.parent.p3.hot_spot_Frame.vent_frame_list[0].ye_variable.min_scale_value.get()
			xmax_value = self.parent.parent.parent.page.parent.p3.hot_spot_Frame.vent_frame_list[0].xe_variable.max_scale_value.get()
			ymax_value = self.parent.parent.parent.page.parent.p3.hot_spot_Frame.vent_frame_list[0].ye_variable.max_scale_value.get()
			self.update_scale(new_vent.xe_vertex_scale, xmin_value, xmax_value)
			self.update_scale(new_vent.ye_vertex_scale, ymin_value, ymax_value)
			self.load_forest_button.configure(command=self.parent.LoadRegion)
		self.vent_frame_list.append(new_vent)
	def increment_nb_vents(self):
		s=self.nb_vents.get()+1
		self.nb_vents.set(s)
	def decrement_nb_vents(self):
		s=self.nb_vents.get()-1
		self.nb_vents.set(s)
	def command_button(self):
		self.increment_nb_vents()
		self.add_vent()
		self.add_vent_button.grid_forget()
		self.draw_region_button.grid_forget()
		if (self.vent_region_vertex in ["vent","vertex"]):
			self.add_vent_button.grid(row = self.nb_vents.get()+1, column = 0, padx = 5, pady = 5)
			if self.vent_region_vertex=="vertex":
				self.load_forest_frame.grid(row = self.nb_vents.get()+1, column = 1, padx = 5, pady = 5)
		if (self.vent_region_vertex=="region"):
			self.add_vent_button.grid(row = self.nb_vents.get()+1, column = 0, padx = 5, pady = 20)
			self.draw_region_button.grid(row = self.nb_vents.get()+1, column = 1, padx = 5, pady = 20)
	def update_vent_nbs(self):
		for n in range(len(self.vent_frame_list)):
			self.vent_frame_list[n].vent_nb_variable.set(n+1)
			if (self.vent_region_vertex in ["vent","region"]):
				self.vent_frame_list[n].configure(text=self.vent_region_vertex+" "+str(n+1))
			if (self.vent_region_vertex == "vertex"):
				self.vent_frame_list[n].Label_vertex_nb.configure(text=self.vent_region_vertex+" "+str(n+1)+" (UTM - E,N)")
			self.vent_frame_list[n].grid_forget()
			self.vent_frame_list[n].grid(row = n+1, column = 0)   #peut etre eviter si pas de soucis de positionnement avc add
