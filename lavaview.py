#
# This file is part of Shalava.
#
# Copyright (C) 2019 Noe Bernabeu <Noe.Bernabeu@gmail.com>
#
# Shalava is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Shalava is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Shalava; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# =========================================================================
# script bonjour.py
from tkinter import *
from tkinter import ttk
from canvas import *
import os

def run_simulation():
	print(nb_var.get()-1)
	my_file = open("file.txt", "w") # Argh j'ai tout écrasé !
	my_file.write("#include \"input.h\""+ "\n\n")
	my_file.write("/* --------- Simulation domain --------- */\n")
	my_file.write("south = "+str(south_var.get())+ ";\n")
	my_file.write("est = "+str(est_var.get())+ ";\n") 
	my_file.write("north = "+str(north_var.get())+ ";\n")
	my_file.write("west = "+str(west_var.get())+ ";\n")
	my_file.write("reso = "+str(reso_var.get())+ ";\n\n")
	my_file.write("/* --------- Hot spot caracteristics (circular vents) ---------*/\n\n")
	my_file.write("nb_vents = %d"%nb_var.get() + ";\n\n")
	for i in range(int(nb_var.get())):
		my_file.write("// Caracteristics of vent %d"%(i+1) + "\n")
		my_file.write("Q[%d"%(i+1)+"] = "+str(Q_list_Label[i].get())+";\n")
		my_file.write("R[%d"%(i+1)+"] = "+str(R_list_Label[i].get())+";\n")
		my_file.write("xe[%d"%(i+1)+"] = "+"point("+str(xe_list_Label[i].get())+","+str(ye_list_Label[i].get())+");\n")
		my_file.write("te[%d"%(i+1)+"] = "+str(ti_list_Label[i].get())+";\n")
		my_file.write("tf[%d"%(i+1)+"] = "+str(tf_list_Label[i].get())+";\n\n")
	if int(varGr.get())>1:
		my_file.write("/* ---------Forest caracteristics ---------*/\n\n")
		if int(varGr.get())==2:
			my_file.write("presence_of_trees = \"everywhere\";\n\n")
		else:
			my_file.write("presence_of_trees = \"region\";\n\n")
		my_file.write("d = "+str(d_var.get())+ ";\n")
		my_file.write("M = "+str(M_var.get())+ ";\n")
		if int(arrangement_var.get())==1:
			my_file.write("arrangement_of_trees = square;")
		else:
			my_file.write("arrangement_of_trees = hexagonal;")
	my_file.close()

def rm_vents(i):
	global Q_list_Label
	global R_list_Label
	global xe_list_Label
	global ye_list_Label
	global ti_list_Label
	global tf_list_Label
	global Frame_list_Label
	global rm_vent_Button

	s=nb_var.get()-1
	nb_var.set(s)
	
	del Q_list_Label[i]
	del R_list_Label[i]
	del xe_list_Label[i]
	del ye_list_Label[i]
	del ti_list_Label[i]
	del tf_list_Label[i]
	Frame_list[i].destroy()
	del Frame_list[i]
	rm_vent_Button = ttk.Button(Frame_list[i-1], text = 'X', image=photo, command = lambda x = i-1:rm_vents(x))
	rm_vent_Button.grid(row = 0, column = 0)
	if nb_var.get()==1:
		rm_vent_Button.destroy()


def add_vents():
	global Q_list_Label
	global R_list_Label
	global xe_list_Label
	global ye_list_Label
	global ti_list_Label
	global tf_list_Label
	global Frame_list_Label
	global rm_vent_Button
	global add_vent_Button
	s=nb_var.get()+1
	nb_var.set(s)
	rm_vent_Button.destroy()
	Frame_s = ttk.LabelFrame(Frame1, text='Vent %d' % s, borderwidth=2, relief=GROOVE)
	Frame_s.grid(row = 1+s, column = 0)

	ttk.Label(Frame_s, text="Q = ").grid(row = 1, column = 0)
	ttk.Label(Frame_s, text='R = ').grid(row = 1, column = 2)
	ttk.Label(Frame_s, text='xe = ').grid(row = 2, column = 0)
	ttk.Label(Frame_s, text='ye = ').grid(row = 2, column = 2)
	ttk.Label(Frame_s, text='ti = ').grid(row = 3, column = 0)
	ttk.Label(Frame_s, text='tf = ').grid(row = 3, column = 2)

	Q_var = DoubleVar()
	R_var = DoubleVar()
	xe_var = DoubleVar()
	ye_var = DoubleVar()
	ti_var = DoubleVar()
	tf_var = DoubleVar()

	Q_Entry = ttk.Entry(Frame_s, textvariable=Q_var)
	R_Entry = ttk.Entry(Frame_s, textvariable=R_var)
	xe_Entry = ttk.Entry(Frame_s, textvariable=xe_var)
	ye_Entry = ttk.Entry(Frame_s, textvariable=ye_var)
	ti_Entry = ttk.Entry(Frame_s, textvariable=ti_var)
	tf_Entry = ttk.Entry(Frame_s, textvariable=tf_var)

	Q_Entry.grid(row = 1, column = 1)
	R_Entry.grid(row = 1, column = 3)
	xe_Entry.grid(row = 2, column = 1)
	ye_Entry.grid(row = 2, column = 3)
	ti_Entry.grid(row = 3, column = 1)
	tf_Entry.grid(row = 3, column = 3)

	rm_vent_Button = ttk.Button(Frame_s, text = 'X', image=photo, command = lambda x = s-1:rm_vents(x))
	rm_vent_Button.grid(row = 0, column = 0)
	
	Q_list_Label.append(Q_var)
	R_list_Label.append(R_var)
	xe_list_Label.append(xe_var)
	ye_list_Label.append(ye_var)
	ti_list_Label.append(ti_var)
	tf_list_Label.append(tf_var)
	Frame_list.append(Frame_s)
	add_vent_Button.destroy()
	add_vent_Button = ttk.Button(Frame1, text = 'Add a vent', command = add_vents)
	add_vent_Button.grid(row = nb_var.get()+2, column = 0)

def open_region_frame():
	global forest_caracteristics_Frame
	global region_Frame
	global square_Radio
	global polygon_Frame
	region_Frame.destroy()
	polygon_Frame.destroy()
	region_Frame = ttk.LabelFrame(forest_caracteristics_Frame, text="Tree configuration", borderwidth=2, relief=GROOVE)
	region_Frame.grid(sticky="W", row = 1, column = 2)
	d_Label = ttk.Label(region_Frame, text = "d =")
	M_Label = ttk.Label(region_Frame, text = "M =")
	packing_Label = ttk.Label(region_Frame, text = "Tree packing:")
	d_Entry = ttk.Entry(region_Frame, textvariable=d_var)
	M_Entry = ttk.Entry(region_Frame, textvariable=M_var)
	square_Radio = Radiobutton(region_Frame, variable=arrangement_var, text = "Square", value=1)
	#square_Radio.select()
	exa_Radio = Radiobutton(region_Frame, variable=arrangement_var, text = "Exagonal", value=2)
	d_Label.grid(row = 0, column = 0)
	M_Label.grid(row = 1, column = 0)
	packing_Label.grid(row = 2, column = 0)
	d_Entry.grid(row = 0, column = 1)
	M_Entry.grid(row = 1, column = 1)
	square_Radio.grid(row = 2, column = 1)
	exa_Radio.grid(row = 2, column = 2)
	if int(varGr.get())==3:
		polygon_Frame = ttk.LabelFrame(forest_caracteristics_Frame, text="Polygon", borderwidth=2, relief=GROOVE)
		polygon_Frame.grid(sticky="W", row = 2, column = 2)
		sommit_Label = ttk.Label(polygon_Frame, text = "sommit")
		sommit_Label.grid(row = 0, column = 0)


def close_region_frame():
	global region_Frame
	global polygon_Frame
	region_Frame.destroy()
	polygon_Frame.destroy()

# Declaration of main window

root = Tk()
root.title("Lavaview")
root.resizable(width=False, height=False)
#root.attributes('-fullscreen', 1)

scrollable_canvas = ScrollableCanvas(root)
scrollable_canvas.grid(row=0,column=0)  
main_window = scrollable_canvas.interior

# Configuration du gestionnaire de grille
#main_window.rowconfigure(0, weight=1)
#main_window.columnconfigure(0, weight=1)"""

# A frame about the simulation domain
simulation_domain_Frame = ttk.LabelFrame(main_window, text="Simulation domain", borderwidth=2, relief=GROOVE)
simulation_domain_Frame.grid(sticky="W", row = 0, column = 0)
south_Label = ttk.Label(simulation_domain_Frame, text = "south =")
est_Label = ttk.Label(simulation_domain_Frame, text = "est =")
north_Label = ttk.Label(simulation_domain_Frame, text = "north =")
west_Label = ttk.Label(simulation_domain_Frame, text = "west =")
reso_Label = ttk.Label(simulation_domain_Frame, text = "reso =")
south_var = DoubleVar()
est_var = DoubleVar()
north_var = DoubleVar()
west_var = DoubleVar()
reso_var = DoubleVar()
south_Entry = ttk.Entry(simulation_domain_Frame, textvariable=south_var)
est_Entry = ttk.Entry(simulation_domain_Frame, textvariable=est_var)
north_Entry = ttk.Entry(simulation_domain_Frame, textvariable=north_var)
west_Entry = ttk.Entry(simulation_domain_Frame, textvariable=west_var)
reso_Entry = ttk.Entry(simulation_domain_Frame, textvariable=reso_var)
south_Label.grid(row = 0, column = 0)
est_Label.grid(row = 1, column = 0)
north_Label.grid(row = 2, column = 0)
west_Label.grid(row = 3, column = 0)
reso_Label.grid(row = 4, column = 0)
south_Entry.grid(row = 0, column = 1)
est_Entry.grid(row = 1, column = 1)
north_Entry.grid(row = 2, column = 1)
west_Entry.grid(row = 3, column = 1)
reso_Entry.grid(row = 4, column = 1)

# A frame about the hot spot caracteristics
vents_list_Label = list()
Q_list_Label = list()
R_list_Label = list()
xe_list_Label = list()
ye_list_Label = list()
ti_list_Label = list()
tf_list_Label = list()
Frame_list = list()

photo = PhotoImage(file='cross.gif')
Frame1 = ttk.LabelFrame(main_window, text="Hot spot caracteristics", borderwidth=2, relief=GROOVE)
Frame1.grid(sticky="W", row = 1, column =0)
nb_var = IntVar()
nb_var.set(1)

nb_Label = ttk.Label(Frame1, text = "nb_vents =")
nb_Entry = ttk.Label(Frame1, textvariable=nb_var)
nb_Label.grid(row = 0, column = 0)
nb_Entry.grid(row = 0, column = 1)
Frame_1 = ttk.LabelFrame(Frame1, text='Vent %d' % 1, borderwidth=2, relief=GROOVE)
Frame_1.grid(row = 1, column = 0)

ttk.Label(Frame_1, text="Q = ").grid(row = 0, column = 0)
ttk.Label(Frame_1, text='R = ').grid(row = 0, column = 2)
ttk.Label(Frame_1, text='xe = ').grid(row = 1, column = 0)
ttk.Label(Frame_1, text='ye = ').grid(row = 1, column = 2)
ttk.Label(Frame_1, text='ti = ').grid(row = 2, column = 0)
ttk.Label(Frame_1, text='tf = ').grid(row = 2, column = 2)

Q_var = DoubleVar()
R_var = DoubleVar()
xe_var = DoubleVar()
ye_var = DoubleVar()
ti_var = DoubleVar()
tf_var = DoubleVar()

Q_Entry = ttk.Entry(Frame_1, textvariable=Q_var)
R_Entry = ttk.Entry(Frame_1, textvariable=R_var)
xe_Entry = ttk.Entry(Frame_1, textvariable=xe_var)
ye_Entry = ttk.Entry(Frame_1, textvariable=ye_var)
ti_Entry = ttk.Entry(Frame_1, textvariable=ti_var)
tf_Entry = ttk.Entry(Frame_1, textvariable=tf_var)

Q_Entry.grid(row = 0, column = 1)
R_Entry.grid(row = 0, column = 3)
xe_Entry.grid(row = 1, column = 1)
ye_Entry.grid(row = 1, column = 3)
ti_Entry.grid(row = 2, column = 1)
tf_Entry.grid(row = 2, column = 3)

add_vent_Button = ttk.Button(Frame1, text = 'Add a vent', command = add_vents)
add_vent_Button.grid(row = nb_var.get()+2, column = 0)

rm_vent_Button = ttk.Button(Frame_1)

Q_list_Label.append(Q_var)
R_list_Label.append(R_var)
xe_list_Label.append(xe_var)
ye_list_Label.append(ye_var)
ti_list_Label.append(ti_var)
tf_list_Label.append(tf_var)
Frame_list.append(Frame_1)

# A frame about the hot spot caracteristics
d_var = DoubleVar()
M_var = DoubleVar()

# A frame about forest caracteristics
forest_caracteristics_Frame = ttk.LabelFrame(main_window, text="Forest caracteristics", borderwidth=2, relief=GROOVE)
forest_caracteristics_Frame.grid(sticky="W", row =2, column = 0)
arrangement_var = StringVar()
region_Frame = ttk.LabelFrame(forest_caracteristics_Frame, text="Tree configuration", borderwidth=2, relief=GROOVE)
polygon_Frame = ttk.LabelFrame(forest_caracteristics_Frame, text="Polygon", borderwidth=2, relief=GROOVE)
square_Radio = Radiobutton(region_Frame, variable=arrangement_var, text = "Square", value=1)
square_Radio.select()

presence_Label = Label(forest_caracteristics_Frame, text = "Presence of forest: ")
varGr = StringVar()
no_Radio = Radiobutton(forest_caracteristics_Frame, variable=varGr, text = "No", value=1, command= close_region_frame)
everywhere_Radio = Radiobutton(forest_caracteristics_Frame, variable=varGr, text = "Everywhere", value=2, command= open_region_frame)
region_Radio = Radiobutton(forest_caracteristics_Frame, variable=varGr, text = "Region", value=3, command= open_region_frame)
presence_Label.grid(row = 0, column = 0)
no_Radio.grid(row = 0, column = 1)
everywhere_Radio.grid(row = 0, column = 2)
region_Radio.grid(row = 0, column = 3)
no_Radio.select()

# Creation of Buttons
quit_Button = ttk.Button(main_window, text = 'Quit', command = root.destroy)
quit_Button.grid(row = 3, column = 1)

run_simulation_Button = ttk.Button(main_window, text = 'Run simulation', command = run_simulation)
run_simulation_Button.grid(row = 3, column = 0)

root.mainloop()
os.system('make')
